﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using TcptR4DataOperations.Model;
using TcptR4DataOperations.Model.BeneficiaryHold;
using TcptR4Services.Model;

namespace TcptR4Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TCPTR4Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TCPTR4Service.svc or TCPTR4Service.svc.cs at the Solution Explorer and start debugging.
    public class TCPTR4Service : ITCPTR4Service
    {

        protected static readonly ILog appLogger = LogManager.GetLogger(typeof(TCPTR4Service));
        //protected static readonly ILog WATAppLogger = LogManager.GetLogger(typeof(TCPTR4Service));
        //Search Beneficiary by paramters
        public TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchByParamsDirect(Model.BeneficiaryRequestModel beneficiarySearchchildobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.beneficiarySearchByParams(beneficiarySearchchildobj);
        }

        public List<TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNResponse> BeneficiaryReservationsFromRN(TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNModel icpSearchObj)
        {
            TcptR4DataOperations.TcptR4Operation Obj = new TcptR4DataOperations.TcptR4Operation();
            return Obj.BenificieryReservationSearchFromRN(icpSearchObj);
        } 

        //Search Specific Beneficiary by needkey or global id
        /*        public List<ChildDataModel> specificBeneficiarySearch(string needKey,string globalId,string nomoney, string sponsorshipAct)
                {
                    TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
                    return obj.specificBeneficiarySearch(needKey, globalId, nomoney, sponsorshipAct);
                }
        */
        // search specific beneficiary method
        public List<ChildDataModel> specificBeneficiarySearch(SpecificBeneficiarySearchModel searchObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.specificBeneficiarySearch(searchObj);
        }

        public List<TcptR4DataOperations.Model.ChildDataModel> specificBeneficiarySearchNew(TcptR4DataOperations.Model.SpecificBeneficiarySearchModel searchObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.specificBeneficiarySearch(searchObj);
        }

        //Search Specific Beneficiary Hold data
        public List<TcptR4DataOperations.Model.AddChild.BeneficiaryDataModel> GetUKHoldedChild(TcptR4DataOperations.Model.AddChild.BeneficiaryDataModel childobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.GetHoldedChild(childobj);
        }

        //Search Beneficiary by paramters and Hold them
        public TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchAndHold(Model.BeneficiaryRequestModel beneficiarySearchchildobj, TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            if (childreleaseobj != null)
            {
                obj.ReleaseBeneficiaryHold(childreleaseobj);
            }
            TcptR4DataOperations.Model.BeneficiarySearch.Response benSearchRes = obj.beneficiarySearchByParams(beneficiarySearchchildobj);
            List<TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist> childListToHold = new List<Beneficiaryholdrequestlist>();
            //TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childtoHold = new Beneficiaryholdrequestlist[benSearchRes.BeneficiarySearchResponseList.Length];

            for (int i = 0; i < benSearchRes.BeneficiarySearchResponseList.Length; i++)
            {
                TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist childtoHold = new Beneficiaryholdrequestlist();
                childtoHold.BeneficiaryState = "E-Commerce Hold";
                childtoHold.Beneficiary_GlobalID = benSearchRes.BeneficiarySearchResponseList[i].Beneficiary_GlobalID;
                childtoHold.HoldEndDate = DateTime.Now;// DateTime.UtcNow.AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ssZ");//DateTime.Now.AddMinutes(1); //;//
                childtoHold.IsSpecialHandling = false;
                childtoHold.PrimaryHoldOwner = "Compassion UK";
                childtoHold.SecondaryHoldOwner = "Compassion UK";
                childtoHold.GlobalPartner_ID = "CU";
                childListToHold.Add(childtoHold);
            }
            TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse childholdres = obj.HoldBeneficiary(childListToHold.ToArray());
            if (childholdres.BeneficiaryHoldResponseList != null)
            {
                for (int i = 0; i < childholdres.BeneficiaryHoldResponseList.Length; i++)
                {
                    benSearchRes.BeneficiarySearchResponseList[i].HoldId = childholdres.BeneficiaryHoldResponseList[i].HoldID;
                }
            }
            return benSearchRes;
        }

        //search beneficiaries by using get method (SEF interface)
        public TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchSEF(Model.BeneficiaryRequestModel beneficiarySearchchildobj, string primaryOwner, string secondaryOwner, TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            if (childreleaseobj != null)
            {
                obj.ReleaseBeneficiaryHold(childreleaseobj);
            }
            TcptR4DataOperations.Model.BeneficiarySearch.Response benSearchRes = obj.beneficiarySearchForSEF(beneficiarySearchchildobj);
            //string benSearchRes = obj.beneficiarySearchForSEF(beneficiarySearchchildobj);
            List<Beneficiaryholdrequestlist> childListToHold = new List<Beneficiaryholdrequestlist>();
            if (benSearchRes.BeneficiarySearchResponseList != null)
            {
                for (int i = 0; i < benSearchRes.BeneficiarySearchResponseList.Length; i++)
                {
                    TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist childtoHold = new Beneficiaryholdrequestlist();
                    childtoHold.BeneficiaryState = "E-Commerce Hold";
                    childtoHold.Beneficiary_GlobalID = benSearchRes.BeneficiarySearchResponseList[i].Beneficiary_GlobalID;
                    childtoHold.HoldEndDate = DateTime.Now.AddMinutes(15);// DateTime.UtcNow.AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ssZ");//DateTime.Now.AddMinutes(1); //;//
                    childtoHold.IsSpecialHandling = false;
                    childtoHold.PrimaryHoldOwner = primaryOwner;//"Compassion UK";
                    childtoHold.SecondaryHoldOwner = secondaryOwner;//"Compassion UK";
                    childtoHold.GlobalPartner_ID = "CU";
                    childtoHold.SourceCode = "Unknown - SEF";
                    childListToHold.Add(childtoHold);
                    //if (string.IsNullOrWhiteSpace(benSearchRes.BeneficiarySearchResponseList[i].BirthDate))
                    //{
                    //    DateTime BirthDate = Convert.ToDateTime(benSearchRes.BeneficiarySearchResponseList[i].BirthDate);
                    //    BirthDate.ToShortDateString();

                    //    DateTime now = DateTime.Now;
                    //    DateTime myDate = BirthDate;
                    //    int age = (int)Math.Floor((DateTime.Now - myDate).TotalDays / 365.25D);
                    //    //float calendar_years = now.Month - myDate.Month;
                    //    benSearchRes.BeneficiarySearchResponseList[i].Age = Convert.ToString(age);
                    //}
                }
                TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse childholdres = obj.HoldBeneficiary(childListToHold.ToArray());
                if (childholdres.BeneficiaryHoldResponseList != null)
                {
                    for (int i = 0; i < childholdres.BeneficiaryHoldResponseList.Length; i++)
                    {
                        benSearchRes.BeneficiarySearchResponseList[i].HoldId = childholdres.BeneficiaryHoldResponseList[i].HoldID;

                    }
                }
            }
            return benSearchRes;
        }

        //Hold Beneficiaries
        /*public TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldBeneficiary(BeneficiaryHoldRequestModel childobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.HoldBeneficiary(childobj);
        }*/

        public TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldBeneficiaries(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.HoldBeneficiary(childobj);
        }

        public void ReleaseBeneficiariesHold(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.ReleaseBeneficiaryHold(childobj);
        }

        public TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj)
        {
            //long ChildRNID = 0;
            long NeedId = 0;
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse holdResult = obj.HoldBeneficiary(childholdobj);

            //start new changes for update and hold message
            if (holdResult.Status == 200 || holdResult.Status == 207)
            {
                for (int i = 0; i < holdResult.BeneficiaryHoldResponseList.Length; i++)
                {
                    if (holdResult.BeneficiaryHoldResponseList[i].Code == 2000)
                    {
                        if (!string.IsNullOrEmpty(holdResult.BeneficiaryHoldResponseList[i].HoldID))
                        {
                            childholdobj[i].HoldID = holdResult.BeneficiaryHoldResponseList[i].HoldID;
                            NeedId = obj.fetchNeedIdByGlobalId(holdResult.BeneficiaryHoldResponseList[i].Beneficiary_GlobalID);
                            //if (NeedId == 0)
                            //
                            childimportobj[i].needStatus = 9;
                            NeedId = obj.AddChildInRN(childimportobj[i]);
                            // }
                            obj.AddBeneciaryHoldRequestinRN(childholdobj[i], NeedId);
                            //ChildRNID = obj.AddChildInRN(childimportobj[i]);
                            //if (ChildRNID == 0)
                            //{
                            //    //call update method here
                            //    long NeedRNID = obj.UpdateChildRNNew(childimportobj[i]);
                            //    if (NeedRNID != 0)
                            //    {
                            //        obj.AddBeneciaryHoldRequestinRN(childholdobj[i], NeedRNID);
                            //    }
                            //}
                            //else
                            //{
                            //    obj.AddBeneciaryHoldRequestinRN(childholdobj[i], ChildRNID);
                            //}
                            //Mainwebservice mainserviceclient = new Mainwebservice();
                            SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                            childobj.actionid = 1004;
                            childobj.globalid = childholdobj[i].Beneficiary_GlobalID;
                            this.SendDataToMainWS(childobj);
                        }
                        /* else
                         {
                             SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                             childobj.actionid = 1004;
                             childobj.globalid = childholdobj[i].Beneficiary_GlobalID;
                             this.SendDataToMainWS(childobj);
                         }*/
                    }

                }
                //end new changes for update and hold message

            }
            return holdResult;
        }

        //public void ReleaseAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj, TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj)
        //public long ReleaseAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj, TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj)

        public BeneficiaryHoldResponse ReleaseAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj, TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            if (childreleaseobj != null)
            {
                obj.ReleaseBeneficiaryHold(childreleaseobj);
            }
            //TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse holdResult = obj.UpdateBeneficiaryHold(childholdobj);
            TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse holdResult = obj.UpdateMultippleHold(childholdobj);
            if (holdResult.BeneficiaryHoldResponseList[0].Code == 2000)
            {
                long[] ChildRNID = obj.AddChildRN(childimportobj);
                if (ChildRNID[0] != 0)
                {
                    holdResult.BeneficiaryHoldResponseList[0].Beneficiary_LocallID = childimportobj[0].localId;
                    holdResult.BeneficiaryHoldResponseList[0].ChildRNID = ChildRNID[0];
                    childholdobj[0].HoldID = holdResult.BeneficiaryHoldResponseList[0].HoldID;
                    //childholdobj[0].Message = holdResult.BeneficiaryHoldResponseList[0].Message;
                    //childholdobj[0].Code = holdResult.BeneficiaryHoldResponseList[0].Code.ToString();
                    obj.AddBeneciaryHoldRequestinRN(childholdobj[0], ChildRNID[0]);
                    SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                    childobj.actionid = 1004;
                    childobj.globalid = childholdobj[0].Beneficiary_GlobalID;
                    this.SendDataToMainWS(childobj);
                    //obj.AddBeneciaryHoldResponseinRN(childholdobj[i]);
                }
                else
                {
                    ChildDataModel childData = new ChildDataModel();
                    childData = obj.FetchChildDataFromRN(childholdobj[0].Beneficiary_GlobalID);
                    holdResult.BeneficiaryHoldResponseList[0].Beneficiary_LocallID = childimportobj[0].localId;
                    holdResult.BeneficiaryHoldResponseList[0].ChildRNID = Convert.ToInt32(childData.NeedRNID);
                    childholdobj[0].HoldID = holdResult.BeneficiaryHoldResponseList[0].HoldID;
                    obj.AddBeneciaryHoldRequestinRN(childholdobj[0], Convert.ToInt32(childData.NeedRNID));
                    holdResult.BeneficiaryHoldResponseList[0].HoldID = holdResult.BeneficiaryHoldResponseList[0].HoldID;
                    //childholdobj[0].Message = holdResult.BeneficiaryHoldResponseList[0].Message;
                    //childholdobj[0].Code = holdResult.BeneficiaryHoldResponseList[0].Code.ToString();                    
                    // SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                    // childobj.globalid = childholdobj[0].Beneficiary_GlobalID;
                }
            }
            return holdResult;
        }

        // Oneview Release Beneficiary Hold method
        public TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse ReleaseBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.ChildDataModel childdata = new TcptR4DataOperations.Model.ChildDataModel();
            for (int i = 0; i < childreleaseobj.Length; i++)
            {
                childdata = obj.FetchChildDataFromRN(childreleaseobj[i].Beneficiary_GlobalID);
                childreleaseobj[i].HoldID = childdata.HoldID;

            }

            TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse response = obj.ReleaseBeneficiaryHold(childreleaseobj);

            if (response.Status == 200 || response.Status == 207)
            {
                for (int j = 0; j < response.BeneficiaryHoldReleaseResponseList.Length; j++)
                {
                    if (response.BeneficiaryHoldReleaseResponseList[j].Code == 2000)
                    {
                        obj.UpdateBeneciaryHoldReleaseinRN(response.BeneficiaryHoldReleaseResponseList[j].Beneficiary_GlobalID);
                        TcptR4DataOperations.Model.AddChild.AddChildRequest childobj = new TcptR4DataOperations.Model.AddChild.AddChildRequest();
                        childobj.needStatus = 278;  // "UK Hold (Inactive)”
                        obj.UpdateNeedByGlobalID(childobj, response.BeneficiaryHoldReleaseResponseList[j].Beneficiary_GlobalID);
                    }
                }
            }

            return response;
        }

        //1.15	Cancel Beneficiary Reservation
        public TcptR4DataOperations.Model.BeneficiaryReleaseReservation.BenReservationReleaseResponse CancelBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReleaseReservation.ReservationCancelRequestList[] childreleaseobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.ChildDataModel childdata = new TcptR4DataOperations.Model.ChildDataModel();
            long ReservationId = 0;
            string Beneficiary_GlobalID = "";
            for (int i = 0; i < childreleaseobj.Length; i++)
            {
                childdata = obj.fetchBenReservationData(childreleaseobj[i].Beneficiary_GlobalID);
                Beneficiary_GlobalID = childreleaseobj[i].Beneficiary_GlobalID;
                ReservationId = obj.fetchReservationIDByGlobalId(Beneficiary_GlobalID);
                childreleaseobj[i].Reservation_ID = childdata.ReservationID;
            }
            //Reservation Number :RES0000077321 is Cancelled
            TcptR4DataOperations.Model.BeneficiaryReleaseReservation.BenReservationReleaseResponse response = obj.CancelBeneficiaryReservation(childreleaseobj);

            if (response.Status == 200 || response.Status == 207)
            {
                for (int i = 0; i < response.ReservationCancelResponseList.Length; i++)
                {
                    if (response.ReservationCancelResponseList[i].Code == 2000)
                    {
                        obj.updateChildReservationStatus(ReservationId);
                    }
                }
            }

            return response;
        }

        // update beneficiary for no money hold from sef
        public TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse UpdateBenHoldSEF(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, string ChildRNID)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse holdResult = obj.UpdateMultippleHold(childholdobj);
            long rnid = obj.fetchNeedIdByGlobalId(childholdobj[0].Beneficiary_GlobalID);
            if (holdResult.BeneficiaryHoldResponseList[0].Code == 2000)
            {
                obj.UpdateBeneciaryHoldRequestinRN(childholdobj[0], rnid);
                TcptR4DataOperations.Model.AddChild.AddChildRequest childobj = new TcptR4DataOperations.Model.AddChild.AddChildRequest();
                childobj.needStatus = 258;
                obj.UpdateNeedByGlobalID(childobj, childholdobj[0].Beneficiary_GlobalID);
            }

            return holdResult;
        }

        public BeneficiaryHoldResponse UpdateMultippleBenHold(Beneficiaryholdrequestlist[] childholdobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.TcptR4Operation rnobj = new TcptR4DataOperations.TcptR4Operation();
            ChildDataModel childdata = new ChildDataModel();
            //     List<ChildDataModel> rnholdobj = rnobj.FetchChildFromRN(childholdobj);

            string[] NeedRNID = new string[childholdobj.Length];

            for (int i = 0; i < childholdobj.Length; i++)
            {
                childdata = rnobj.FetchChildDataFromRN(childholdobj[i].Beneficiary_GlobalID);
                childholdobj[i].HoldID = childdata.HoldID;
                //childholdobj[i].Beneficiary_LocalID = childdata.Beneficiary_LocalID;
                NeedRNID[i] = childdata.NeedRNID;
            }

            BeneficiaryHoldResponse holdUpdateResult = obj.UpdateMultippleHold(childholdobj);

            for (int i = 0; i < holdUpdateResult.BeneficiaryHoldResponseList.Length; i++)
            {
                obj.UpdateWaitingdate(childholdobj[i].WaitingSinceDate, Convert.ToInt32(NeedRNID[i]));
                //obj.AddChildInRN(childholdobj[i]);
                //childholdobj[i].HoldID = holdUpdateResult.BeneficiaryHoldResponseList[i].HoldID;
                //childholdobj[i].Message = holdUpdateResult.BeneficiaryHoldResponseList[i].Message;
                //childholdobj[i].Code = holdUpdateResult.BeneficiaryHoldResponseList[i].Code.ToString();
                //obj.UpdateMultippleBeneciaryHoldinRN(childholdobj[i]);
                //holdUpdateResult.BeneficiaryHoldResponseList[i].Beneficiary_LocallID = childholdobj[i].Beneficiary_LocalID;
                if (childholdobj[i].Beneficiary_GlobalID == holdUpdateResult.BeneficiaryHoldResponseList[i].Beneficiary_GlobalID && holdUpdateResult.BeneficiaryHoldResponseList[i].Code == 2000) //Convert.ToInt32(childholdobj[i].) == 2000)
                {

                    // changes done for updating last hold record
                    // obj.AddBeneciaryHoldRequestinRN(childholdobj[i], Convert.ToInt32(NeedRNID[i]));
                    obj.UpdateBeneciaryHoldRequestinRN(childholdobj[i], Convert.ToInt32(NeedRNID[i]));

                    SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                    childobj.actionid = 1004;
                    childobj.globalid = childholdobj[i].Beneficiary_GlobalID;
                    this.SendDataToMainWS(childobj);
                }
            }
            return holdUpdateResult;
        }

        //public TcptR4DataOperations.Model.ChildDataModel FetchChildDataFromRN(TcptR4DataOperations.Model.ChildDataModel rnchildobj) {
        //    TcptR4DataOperations.Model.ChildDataModel obj = new ChildDataModel();
        //    TcptR4DataOperations.TcptR4Operation childdata = new TcptR4DataOperations.TcptR4Operation();
        //    obj = childdata.FetchChildDataFromRN(rnchildobj); 
        //    return obj;
        //}

        public TcptR4DataOperations.Model.BeneficiaryReservation.Response createBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReservation.Globalpartnerreservationrequestlist[] benReserveRequest, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childreserveobj)
        {
            TcptR4DataOperations.TcptR4Operation beneficiaryReserveObj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.BeneficiaryReservation.Response benResult = beneficiaryReserveObj.CreateBenReservations(benReserveRequest);
            //long[] ChildRNID = beneficiaryReserveObj.AddChildRN(childreserveobj);
            if (benResult.Status == 200 || benResult.Status == 207)
            {
                long rnid = 0;//beneficiaryReserveObj.fetchNeedIdByGlobalId(benReserveRequest[0].Beneficiary_GlobalID);
                for (int i = 0; i < benResult.GlobalPartnerReservationResponseList.Length; i++)
                {
                    if (benResult.GlobalPartnerReservationResponseList[i].Code == 2000)
                    {
                        benReserveRequest[i].Reservation_ID = benResult.GlobalPartnerReservationResponseList[i].Reservation_ID;
                        benReserveRequest[i].Message = benResult.GlobalPartnerReservationResponseList[i].Message;
                        benReserveRequest[i].Code = benResult.GlobalPartnerReservationResponseList[i].Code.ToString();
                        beneficiaryReserveObj.AddBeneficiaryReservation(benReserveRequest[i], rnid);
                    }
                }
            }
            return benResult;
        }

        public TcptR4DataOperations.Model.BeneficiaryReservation.Response updateBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReservation.Globalpartnerreservationrequestlist[] benReserveRequest, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childreserveobj)
        {
            TcptR4DataOperations.TcptR4Operation beneficiaryReserveObj = new TcptR4DataOperations.TcptR4Operation();
            TcptR4DataOperations.Model.ChildDataModel childdata = new TcptR4DataOperations.Model.ChildDataModel();
            //string[] NeedRNID = new string[benReserveRequest.Length];
            long ReservationId = 0;
            for (int i = 0; i < benReserveRequest.Length; i++)
            {
                childdata = beneficiaryReserveObj.fetchBenReservationData(benReserveRequest[i].Beneficiary_GlobalID);
                benReserveRequest[i].ID = childdata.ReservationID;
                ReservationId = beneficiaryReserveObj.fetchReservationIDByGlobalId(benReserveRequest[i].Beneficiary_GlobalID);
                beneficiaryReserveObj.updateChildExpirationDate(Convert.ToInt32(ReservationId), benReserveRequest[i].Channel_Name, benReserveRequest[i].SourceCode, benReserveRequest[i].PrimaryOwner, benReserveRequest[i].SecondaryOwner, benReserveRequest[i].HoldExpirationDate, benReserveRequest[i].ExpirationDate);
                benReserveRequest[i].HoldExpirationDate = childdata.HoldExpirationDate.ToString("yyyy-MM-dd'T'HH:mm:ss.ff'Z'");

                //NeedRNID[i] = childdata.NeedRNID;
            }

            TcptR4DataOperations.Model.BeneficiaryReservation.Response benResult = beneficiaryReserveObj.UpdateBenReservations(benReserveRequest);
            //long[] ChildRNID = null;//beneficiaryReserveObj.AddChildRN(childreserveobj);
            if (benResult.Status == 200 || benResult.Status == 207)
            {
                for (int i = 0; i < benResult.GlobalPartnerReservationResponseList.Length; i++)
                {
                    if (benResult.GlobalPartnerReservationResponseList[i].Code == 2000)
                    {
                        benReserveRequest[i].Reservation_ID = benResult.GlobalPartnerReservationResponseList[i].Reservation_ID;
                        benReserveRequest[i].Message = benResult.GlobalPartnerReservationResponseList[i].Message;
                        benReserveRequest[i].Code = benResult.GlobalPartnerReservationResponseList[i].Code.ToString();
                        //beneficiaryReserveObj.AddBeneficiaryReservation(benReserveRequest[i], Convert.ToInt32(NeedRNID[i]));
                    }
                }
            }
            return benResult;
        }

        /*        public List<TcptR4DataOperations.Model.BeneficiaryReservation.ReservedChildResponselist> GetReservedChild(TcptR4DataOperations.Model.BeneficiaryReservation.ReservedChildRequestlist[] reservedchildobj)
                {
                    TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
                    return obj.GetReservedChild(reservedchildobj);
                }
        */

        public void AddChild(TcptR4DataOperations.Model.AddChild.AddChildRequest[] childobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.AddChildRN(childobj);
        }

        public TcptR4DataOperations.Model.DemandPlanning.Response demandPlanning(string base64data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.demandPlanning(base64data);
        }

        #region ICP Reservation Methods
        public TcptR4DataOperations.Model.ICPSearch.Response icpSearchByParamsDirect(TcptR4DataOperations.Model.ICPSearch.IcpSearchRequestModel icpSearchobj)
        {
            TcptR4DataOperations.TcptR4Operation icpObj = new TcptR4DataOperations.TcptR4Operation();
            return icpObj.IcpSearch(icpSearchobj);
        }

        public TcptR4DataOperations.Model.ChurchPartnerReservation.Response icpReservation(TcptR4DataOperations.Model.ChurchPartnerReservation.Globalpartnerreservationrequestlist[] icpReserveReqObj, TcptR4DataOperations.Model.AddICP.AddICPRequest[] importIcpObj)
        {
            TcptR4DataOperations.TcptR4Operation icpReserveObj = new TcptR4DataOperations.TcptR4Operation();
            //return icpReserveObj.CreateChurchPartnerReservation(icpReserveReqObj);
            for (int i = 0; i < icpReserveReqObj.Length; i++)
            {
                RNReservationDataModel reservationData = new RNReservationDataModel();
                reservationData = icpReserveObj.fetchICPReservationData(icpReserveReqObj[i].ICP_ID);
                icpReserveReqObj[i].ID = ((!string.IsNullOrWhiteSpace(reservationData.ReservationID)) ? reservationData.ReservationID : null);
                icpReserveReqObj[i].CampaignEventIdentifier = ((string.IsNullOrWhiteSpace(icpReserveReqObj[i].CampaignEventIdentifier) && !string.IsNullOrWhiteSpace(reservationData.CampaignEventIdentifier)) ? reservationData.CampaignEventIdentifier : icpReserveReqObj[i].CampaignEventIdentifier);
            }
            TcptR4DataOperations.Model.ChurchPartnerReservation.Response reservationObj = icpReserveObj.CreateChurchPartnerReservation(icpReserveReqObj);
            if (reservationObj.Status == 200 || reservationObj.Status == 207)
            {
                if (reservationObj.GlobalPartnerReservationResponseList != null)
                {
                    for (int i = 0; i < reservationObj.GlobalPartnerReservationResponseList.Length; i++)
                    {
                        if (reservationObj.GlobalPartnerReservationResponseList[i].Code == 2000)
                        {
                            long projectID = icpReserveObj.AddICP(importIcpObj[i]);
                            SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                            childobj.actionid = 1004;
                            childobj.icpid = importIcpObj[i].ICP_ID;
                            this.SendDataToMainWS(childobj);
                            if (projectID != 0)
                            {
                                icpReserveReqObj[i].ID = reservationObj.GlobalPartnerReservationResponseList[i].Reservation_ID;
                                long reservationID = icpReserveObj.AddOrUpdateICPReservation(icpReserveReqObj[i], importIcpObj[i], projectID);
                            }
                        }
                    }
                }
            }
            return reservationObj;
        }

        //public TcptR4DataOperations.Model.ChurchPartnerReservation.Response updateICPReservation(TcptR4DataOperations.Model.ChurchPartnerReservation.Globalpartnerreservationrequestlist[] icpReserveReqObj, TcptR4DataOperations.Model.AddICP.AddICPRequest[] importIcpObj)
        //{
        //    TcptR4DataOperations.TcptR4Operation icpReserveObj = new TcptR4DataOperations.TcptR4Operation();
        //    //return icpReserveObj.CreateChurchPartnerReservation(icpReserveReqObj);
        //    for (int i = 0; i < icpReserveReqObj.Length; i++)
        //    {
        //        string Reservation_id = icpReserveObj.fetchICPReservationData(icpReserveReqObj[i].ICP_ID);
        //        icpReserveReqObj[i].ID = Reservation_id;
        //        //cancelReservationObj[i].ICP_ID = null;
        //    }
        //    TcptR4DataOperations.Model.ChurchPartnerReservation.Response reservationObj = icpReserveObj.CreateChurchPartnerReservation(icpReserveReqObj);
        //    if (reservationObj.Status == 200 || reservationObj.Status == 207)
        //    {
        //        if (reservationObj.GlobalPartnerReservationResponseList != null)
        //        {
        //            for (int i = 0; i < reservationObj.GlobalPartnerReservationResponseList.Length; i++)
        //            {
        //                if (reservationObj.GlobalPartnerReservationResponseList[i].Code == 2000)
        //                {
        //                    long projectID = icpReserveObj.AddICP(importIcpObj[i]);
        //                    SendDatatoEsbModel childobj = new SendDatatoEsbModel();
        //                    childobj.actionid = 1004;
        //                    childobj.icpid = importIcpObj[i].ICP_ID;
        //                    this.SendDataToMainWS(childobj);
        //                    if (projectID != 0)
        //                    {
        //                        icpReserveReqObj[i].ID = reservationObj.GlobalPartnerReservationResponseList[i].Reservation_ID;
        //                        long reservationID = icpReserveObj.AddICPReservation(icpReserveReqObj[i], importIcpObj[i], projectID);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return reservationObj;
        //}

        public TcptR4DataOperations.Model.ICPReleaseReservation.ICPReservationReleaseResponse ReleaseICPReservation(TcptR4DataOperations.Model.ICPReleaseReservation.ReservationCancelRequestList[] cancelReservationObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //TcptR4DataOperations.Model.ChildDataModel childdata = new TcptR4DataOperations.Model.ChildDataModel();            
            for (int i = 0; i < cancelReservationObj.Length; i++)
            {
                RNReservationDataModel reservationData = new RNReservationDataModel();
                reservationData = obj.fetchICPReservationData(cancelReservationObj[i].ICP_ID);
                cancelReservationObj[i].Reservation_ID = ((!string.IsNullOrWhiteSpace(reservationData.ReservationID)) ? reservationData.ReservationID : null);
                //string Reservation_id = obj.fetchICPReservationData(cancelReservationObj[i].ICP_ID);
                //cancelReservationObj[i].Reservation_ID = Reservation_id;
                //cancelReservationObj[i].ICP_ID = null;
            }

            TcptR4DataOperations.Model.ICPReleaseReservation.ICPReservationReleaseResponse response = obj.ReleaseICPReservation(cancelReservationObj);

            if (response.Status == 200 || response.Status == 207)
            {
                for (int j = 0; j < response.ReservationCancelResponseList.Length; j++)
                {
                    if (response.ReservationCancelResponseList[j].Code == 2000)
                    {
                        if (!string.IsNullOrWhiteSpace(cancelReservationObj[j].Reservation_ID))
                        {
                            obj.UpdateIcpReservationReleaseinRN(cancelReservationObj[j].ICP_ID);
                        }
                    }
                }
            }

            return response;
        }

        public List<TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNResponse> ICPReservationsFromRN(TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNModel icpSearchObj)
        {
            TcptR4DataOperations.TcptR4Operation Obj = new TcptR4DataOperations.TcptR4Operation();
            return Obj.ICPReservationSearchFromRN(icpSearchObj);
        }

        #endregion

        public TcptR4DataOperations.Model.SupporterGift.GiftCreateResponse SupporterGiftsUploader(string base64data, string giftType)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.SupporterGiftsUploader(base64data, giftType);
        }

        public List<TcptR4DataOperations.Model.Resubmission.SupporterGroupModel> FetchSupporterGroups(string type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.FetchSupporterGroups(type);
        }

        public List<TcptR4DataOperations.Model.Resubmission.CorrespondenceModel> FetchCorrespondences(string type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.FetchCorrespondence(type);
        }

        public string ResubmitCorrespondeces(TcptR4DataOperations.Model.Resubmission.CorrespondenceModel data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.ResubmitCorrespondence(data);
        }

        public string ResubmitSupporter(string type, TcptR4DataOperations.Model.Resubmission.SupporterGroupModel data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            if (type == "Create")
            {
                return obj.ResubmitCreateSupporter(data);
            }
            else if (type == "Update")
            {
                return obj.ResubmitUpdateSupporter(data);
            }
            return null;
        }

        public List<TcptR4DataOperations.Model.Resubmission.SponsorshipModel> FetchSponsorships(string type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Resubmission.SponsorshipModel> tempList = obj.FetchSponsorships(type);
            return tempList;
        }

        public string ResubmitSponsorship(string type, TcptR4DataOperations.Model.Resubmission.SponsorshipModel data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            if (type == "Create")
            {
                return obj.ResubmitCreateSponsorship(data);
            }
            else if (type == "Cancel")
            {
                return obj.ResubmitCancelCommitment(data);
            }
            return null;
        }

        #region Intervention methods

        public TcptR4DataOperations.Model.Intervention.InterverntionQueryResponse SearchInterventions(string interventionId)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();

            return obj.InterventionSearch(interventionId);

        }

        public TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList createInterventionHoldInGMC(TcptR4DataOperations.Model.Intervention.Interventionholdlist interventionObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Intervention.Interventionholdlist> intHoldobj = new List<TcptR4DataOperations.Model.Intervention.Interventionholdlist>();
            intHoldobj.Add(interventionObj);
            // foreach (TcptR4DataOperations.Model.Intervention.Interventionholdlist intHoldObj in interventionObj) {
            TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList response = new TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList();
            response = obj.createInterventionHoldinGMC(intHoldobj.ToArray());
            // }
            if (response != null && (response.Status == 200 || response.Status == 207))
            {

                for (int i = 0; i < response.InterventionHoldResponse.Length; i++)
                {
                    if (response.InterventionHoldResponse[i].Code == 2000)
                    {
                        // add hold record in RN.
                        interventionObj.HoldID = response.InterventionHoldResponse[i].HoldID;
                        interventionObj.Intervention_ID = response.InterventionHoldResponse[i].Intervention_ID;
                        long intHoldID = obj.AddOrUpdateInterventionHoldInRN(interventionObj, "Held");
                    }
                }
            }
            return response;
        }

        public TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList updateInterventionHold(TcptR4DataOperations.Model.Intervention.Interventionholdlist interventionObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Intervention.Interventionholdlist> intHoldobj = new List<TcptR4DataOperations.Model.Intervention.Interventionholdlist>();
            intHoldobj.Add(interventionObj);
            TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList response = new TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList();

            response = obj.updateInterventionHoldinGMC(intHoldobj.ToArray());

            if (response != null && (response.Status == 200 || response.Status == 207))
            {

                for (int i = 0; i < response.InterventionHoldResponse.Length; i++)
                {
                    if (response.InterventionHoldResponse[i].Code == 2000)
                    {
                        // add hold record in RN.
                        interventionObj.HoldID = response.InterventionHoldResponse[i].HoldID;
                        interventionObj.Intervention_ID = response.InterventionHoldResponse[i].Intervention_ID;
                        long intHoldID = obj.AddOrUpdateInterventionHoldInRN(interventionObj, "Held");
                    }
                }
            }
            return response;
        }

        public TcptR4DataOperations.Model.Intervention.InterventionHoldCancelResponse cancelInterventionHold(TcptR4DataOperations.Model.Intervention.Interventionholdcancellationlist interventionObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Intervention.Interventionholdcancellationlist> cancelObj = new List<TcptR4DataOperations.Model.Intervention.Interventionholdcancellationlist>();
            cancelObj.Add(interventionObj);
            TcptR4DataOperations.Model.Intervention.InterventionHoldCancelResponse response = new TcptR4DataOperations.Model.Intervention.InterventionHoldCancelResponse();

            response = obj.cancelInterventionHoldinGMC(cancelObj.ToArray());

            if (response != null && (response.Status == 200 || response.Status == 207))
            {
                for (int i = 0; i < response.InterventionHoldCancellationResponseList.Length; i++)
                {
                    if (response.InterventionHoldCancellationResponseList[i].Code == 2000)
                    {
                        TcptR4DataOperations.Model.Intervention.Interventionholdlist intObj = new TcptR4DataOperations.Model.Intervention.Interventionholdlist();
                        intObj.Intervention_ID = response.InterventionHoldCancellationResponseList[i].Intervention_ID;
                        intObj.HoldID = response.InterventionHoldCancellationResponseList[i].Intervention_HoldID;
                        obj.AddOrUpdateInterventionHoldInRN(intObj, "Released");
                    }
                }
            }
            return response;
        }

        public TcptR4DataOperations.Model.Intervention.InterventionCommitmentResponse createInterventionCommitment(TcptR4DataOperations.Model.Intervention.Interventioncommitmentlist intCommObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Intervention.Interventioncommitmentlist> commObj = new List<TcptR4DataOperations.Model.Intervention.Interventioncommitmentlist>();
            commObj.Add(intCommObj);

            TcptR4DataOperations.Model.Intervention.InterventionCommitmentResponse response = new TcptR4DataOperations.Model.Intervention.InterventionCommitmentResponse();
            response = obj.createInterventionCommitmentInGMC(commObj.ToArray());

            if (response != null && (response.Status == 200 || response.Status == 207))
            {
                for (int i = 0; i < response.InterventionCommitmentResponseList.Length; i++)
                {
                    TcptR4DataOperations.Model.Intervention.Interventionholdlist intObj = new TcptR4DataOperations.Model.Intervention.Interventionholdlist();
                    intObj.HoldID = response.InterventionCommitmentResponseList[i].HoldID;
                    intObj.Intervention_ID = response.InterventionCommitmentResponseList[i].Intervention_ID;
                    intObj.HoldAmount = intCommObj.Commitment_Amount;
                    intObj.IsRequestedAdditionalFundComm = true;

                    obj.AddOrUpdateInterventionHoldInRN(intObj, "Committed");
                }
            }
            return response;
        }

        public List<TcptR4DataOperations.Model.Intervention.InterventionHoldList> getInterventionHoldsFromRN(TcptR4DataOperations.Model.Intervention.InterventionHoldList intObj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.fetchIntereventionHoldsFromRN(intObj);
        }

        // for fetching data from RN
        public TcptR4DataOperations.Model.Intervention.InterventionHoldList searchInterventionHoldById(int InterventionRNID)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.fetchInterventionHoldFromID(InterventionRNID);
        }

        #endregion

        #region Release Beneficiary Method for packets processed by Oneview Admin

        public TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse CancelBeneficiaryHoldAdminFunction(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();

            TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse response = obj.ReleaseBeneficiaryHold(childreleaseobj);

            return response;
        }

        #endregion

        public void updatecorrespondence()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.updateCorrespondence();
            //obj.TestAppLoggers();
        }

        #region Test methods

        public void testMethod() { }

        #endregion

        // for fetching data from GMC and RN both
        /*public TcptR4DataOperations.Model.Intervention.InterventionHoldList fetchInterventionData(string IntID) {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            List<TcptR4DataOperations.Model.Intervention.InterventionHoldList> rnData = new List<TcptR4DataOperations.Model.Intervention.InterventionHoldList>();

            rnData = obj.fetchIntereventionHoldsFromRN(IntID, null);
            TcptR4DataOperations.Model.Intervention.InterventionHoldList intData = new TcptR4DataOperations.Model.Intervention.InterventionHoldList();

            intData.



        }*/

        #region Method Which Sends Data To Main ESB
        internal void SendDataToMainWS<T>(T obj)
        {
            string jsonpayload = (new JavaScriptSerializer()).Serialize(obj);
            //Mainwebservice.MainServiceClient proxy = new Mainwebservice.MainServiceClient();
            //Action ID is 1004 for child subscriber flow
            TcptR4DataOperations.MainWebService7.MainServicesClient proxy = new TcptR4DataOperations.MainWebService7.MainServicesClient();
            int action = 1004;
            appLogger.Info("Data sent to ESB " + jsonpayload + " at " + DateTime.Now);
            proxy.IAmOneWayMessagingPattern(jsonpayload, action);
        }

        internal void SendDataToMainWS1<T>(T obj)
        {
            string jsonpayload = (new JavaScriptSerializer()).Serialize(obj);
            //Mainwebservice.MainServiceClient proxy = new Mainwebservice.MainServiceClient();
            //Action ID is 1004 for child subscriber flow
            TcptR4DataOperations.MainWebService7.MainServicesClient proxy = new TcptR4DataOperations.MainWebService7.MainServicesClient();
            int action = 1003;
            appLogger.Info("Data sent to ESB " + jsonpayload + " at " + DateTime.Now);
            proxy.IAmOneWayMessagingPattern(jsonpayload, action);
        }

        #endregion

        //counting supporter who dont have webaccesstoken 
        public int SupporterCountToUpdateWebToken()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.supporterCountToUpdateWebToken();
        }
        //sending supporterid to ESB for Generate WebAccess Token
        public string updateWebToken()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            int SupCount = obj.supporterCountToUpdateWebToken();//describe no. of supporter to be generate web access token
            appLogger.Info("The Process of WebTokenUpdate Initiated:" + DateTime.Now.ToLongTimeString());
            appLogger.Info("-------------------------------");
            string file_path = @"C:\Compassion-UK\TokenUpdateService\log-" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            if (!System.IO.File.Exists(file_path))
            {
                var myFile = System.IO.File.Create(file_path);
                myFile.Close();
            }
            using (StreamWriter w = File.AppendText(file_path))
            {
                w.Write("\r\nWeb Access Token for : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            }

            if (SupCount > 0)
            {
                int startlimit = 0;
                while (SupCount > 0)
                {
                    string[] SupporterID = obj.SendingRecordsToWebTokenUpdate(startlimit);
                    if (SupporterID.Length > 0)
                    {
                        foreach (string Supporterid in SupporterID)
                        {
                            SendDatatoEsbModel childobj = new SendDatatoEsbModel();
                            childobj.actionid = 1004;
                            childobj.Supporterid = Supporterid;
                            this.SendDataToMainWS(childobj);
                        }
                        SupCount -= SupporterID.Length;
                        startlimit += SupporterID.Length;

                    }
                }
            }

            using (StreamWriter w = File.AppendText(file_path))
            {
                w.WriteLine("-------------------------------");
                w.WriteLine("The Process End At :" + DateTime.Now.ToLongTimeString());
            }

            appLogger.Info("-------------------------------");
            appLogger.Info("The Process End At :" + DateTime.Now.ToLongTimeString());

            return "Success";
        }

        public int getBeneficiaryCountforAdhocDataRefresh()
        {
            TcptR4DataOperations.TcptR4Operation tcptR4obj = new TcptR4DataOperations.TcptR4Operation();
            return tcptR4obj.getBeneficiaryCountforAdhocDataRefresh();
        }

        public string initiateBeneficiaryAdHocdatarefreshProcess()
        {
            TcptR4DataOperations.TcptR4Operation tcptr4opobj = new TcptR4DataOperations.TcptR4Operation();
            int childcount = tcptr4opobj.getBeneficiaryCountforAdhocDataRefresh();
            if (childcount > 0)
            {
                int startlimit = 0;
                while (childcount > 0)
                {
                    string beneficiaryglobalid = tcptr4opobj.getBeneficiaryGlobalidFromNeed(startlimit);
                    if (!string.IsNullOrWhiteSpace(beneficiaryglobalid) && beneficiaryglobalid != "Null data")
                    {
                        //foreach(string beneficiaryglobalid in beneficiaryglobalids)
                        //{
                        SendDatatoEsbModel sendglobalidtoesb = new SendDatatoEsbModel();
                        sendglobalidtoesb.globalid = beneficiaryglobalid;

                        sendglobalidtoesb.isAdHocDataRefreshPacket = "1";
                        sendglobalidtoesb.actionid = 1004;
                        SendDataToMainWS(sendglobalidtoesb);
                        tcptr4opobj.UpdateAdhocChildStatus(beneficiaryglobalid);

                        //}
                        childcount -= 1;
                        //startlimit += 1;
                    }
                    else
                    {
                        appLogger.Info("Beneficiary global id is missing for child in Need record.");
                    }
                }
            }

            return "Success";
        }

        public string doDataProtectionProcess()
        {
            string bbid = "";
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();

            int SupCount = obj.getdataprotectioncount();
            if (SupCount > 0)
            {
                int startlimit = 0;
                while (SupCount > 0)
                {
                    string[] Supporter = obj.sendingIDandDataprotectionFlag(startlimit);

                    if (Supporter.Length > 0)
                    {
                        foreach (string Supporterdata in Supporter)
                        {
                            string[] contactData = Supporterdata.Split(',');
                            bbid = contactData[2];
                            obj.startDataProtectionProccess(contactData[0], contactData[1]);

                        }
                        SupCount -= Supporter.Length;
                        startlimit += Supporter.Length;

                    }
                }
            }

            return "success";
        }

        public int getDPCount()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            // return obj.getdataprotectioncount();
            return 1;
        }

        public void deleteConnectforDataProtection()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.ConnectforDataProtection("Expire", 0, null);
        }

        public List<TcptR4DataOperations.Model.SupporterDataModel> FetchSupporters(string type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.FetchSupporters(type);
        }

        public string ResubmitSupporters(TcptR4DataOperations.Model.SupporterDataModel data1, string type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.ResubmitSupporters(data1, type);
        }

        

        public bool doBstandingOrder(string base64data, string beforedays, string afterdays, string filename)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doBarclaysStandingOrder(base64data, beforedays, afterdays, filename);
        }

        public bool sendSingleRecordForBarclysStandingOrderProcess(TcptR4DataOperations.Model.BarclaysStandingOrderModel bbobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.sendSingleRecordForBarclysStandingOrderProcess(bbobj.recordid, bbobj.BBID, bbobj.Accountno, bbobj.constituentName, bbobj.Sortcode, bbobj.Amount);
        }

        public bool ScrutinizeGMCOperation()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.ScrutinizeGMCOperation();
        }



        public bool doBacsProcess(string base64data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doBacsProcess(base64data);
        }

        public void SendNotifications(TcptR4DataOperations.Model.NotificationsModel notificationObjs)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.SendNotification(notificationObjs,"", "");//server=172.23.161.10;uid=cukdevco_comapp;" + "pwd=Geecon0404;database=cukdevco_comukapp_v2; convert zero datetime=True;AutoEnlist=false
        }

        public bool doDDRProcess(string base64data, string date)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doDDRProcess(base64data, date);
        }

        public ConstituentRevenueAndGiftAidTotalAmount getGiftaidtotalamountandrevenueperyear(string BBID)
        {
            //BBID = "8-10094210";
            //BBID = "8-10021406";
            //8 - 10006753
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.getGiftaidtotalamountandrevenueperyear(BBID);
        }

        public ICPDetailModel getICPDetails(string ICPDetails)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.FetchICPDetails(ICPDetails);
        }

        public bool DeleteCommitment(string CommitmentID)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.DestroyCommitments(CommitmentID);
        }//createIncidentForAdobeSignature

        public bool createIncidentForAdobeSignatureBeforeSign(string email, string url_for_Travellers, string docket_url_for_Travellers, string url_for_Terms_and_Conditions, string docket_url_for_Terms_and_Conditions)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.createIncidentForAdobeSignatureBeforeSign(email, url_for_Travellers, docket_url_for_Travellers, url_for_Terms_and_Conditions, docket_url_for_Terms_and_Conditions);
        }

        public void createIncidentForAdobeSignatureAfterSign(string email, string url)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.createIncidentForAdobeSignatureAfterSign(email, url);
        }


        public bool doCAFProcess(string base64data, string beforedays, string afterdays, string filename)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doCAFProcess(base64data, beforedays, afterdays, filename);
        }

        public bool doSEFProcess(string base64data, string Type)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doSEFProcess(base64data, Type);
        }

        public void UploadMultipart(byte[] file, string filename, string contentType, string url)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.UploadMultipart(file, filename, contentType, url);
        }

        public void Createincidentwithattachment(createIncidentModel crobj)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            crobj.actionid = 175;
            obj.SendDataToMainWS(crobj, 1000);
        }

        public bool doRateChangeProcess(string base64data, string identifier)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doRateChangeProcess(base64data, identifier);
            // return false;
        }
        //doRateChangeProcess

        public List<DonationModel> getConstituentDonationTableDetails(string lookupid)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.getConstituentDonationTableDetails(lookupid);
        }

        public void DoWhatEverYouWant()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.FindCheckbotRecords();
            //obj.TestDisposition("8-10502443");
            //obj.FetchAllRecurringGiftOtherGroups("8-10502443");
            //obj.FetchSupporterGroupIdForRG(229207);
        }

        public void MajorDonorProcessInDWH()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.MajorDonorProcessInDWH();
        }

        public bool doStewardshipProcess(string base64data,string filename)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doStewardship(base64data, filename);
        }

        public List<SupporterCommitmentModel> GetCommitmentBySupporterGroup(string supGroupId)
        {

            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.SearchCommitmentBySupporterGroup(supGroupId);

        }

        public List<cancelRecurringGift> cancelRecurringGift(List<cancelRecurringGift> rgobj, string delinkReason)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.TestDisposition(delinkReason);
            return obj.CancelRecurringGift(rgobj, delinkReason);
        }

        public void CreateIncidentForcancelRecurringGift(List<cancelRecurringGift> rgobj, string delinkReason, int AssignedAccount)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.TestDisposition(delinkReason);
            obj.CreateIncidentForCancelRecurringGift(rgobj, delinkReason, AssignedAccount);
        }

        public void CreateCancelRGIncidentWithSummary(List<TcptR4DataOperations.Model.RGDetails> CreateRG, List<TcptR4DataOperations.Model.RGDetails> CancelRG)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.CreateCancelRGIncidentWithSummary(648913, "545", CreateRG, CancelRG);
        }

        public List<RecurringGiftModel> FetchRGDetailsOnLookupID(string lookupid, string GroupType, string SupporterGroupID)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.FetchRecurringGiftOnSupporterID(lookupid, GroupType, SupporterGroupID);
        }

        public List<RecurringGiftModel> GetOtherRGOnLookupID(string lookupid)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.GetOtherRGOnLookupID(lookupid);
        }

        public void getdatadone()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.CheckSupporterGroupTypeToDeleteEmailFromHousehold("660462");

        }

        public void getdataDone()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.fetchLastSuccessfulRunTime();
            //obj.updateSupporterGroupPreferredName("252248");
            //obj.UploadAudioFileInBB("8-10123518");
            //obj.fetchSafeguardingLatestActivity("229207");
            //obj.Fetch8x8AllData("7803069818");
            //obj.fetchSupporterDetailsOnPhoneno("7880435187");
            //obj.FetchAccountDetailsFromEnquiry(4612074);
            //obj.DoCreateBatchRB("8-10502811");
            //obj.TM();
            string hiii;
        }
        
        public void FetchNewSponsorship()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.FetchNewSponsorshipSupporters();
        }

        public bool doGDPRprocess(string base64data, int ScopeId)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.doGDPRprocess(base64data, ScopeId);
        }

        public void SendNotification(string Supporterid,string NeedKey,string Env)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.SendNotificationsOperation(Supporterid,NeedKey,Env);
        }

        public long CreateIncidentForCoffee(TcptR4DataOperations.Model.SupporterDataModel supporterData)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CreateIncidentForCoffeeCampaign(supporterData);
        }
        
        public long CreateIncidentForAmbassadors(TcptR4DataOperations.Model.AmbassadorsModal ambassadorsdata)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CreateIncidentForAmbassadorsform(ambassadorsdata);
        }

        public long ReportGenerator()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.reportGenerator();
        }

        public long CreateIncidentForSupPreferredName(TcptR4DataOperations.Model.SupporterDataModel supporterData) 
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CreateIncidentForChangeSupPreferredName(supporterData);
        }

        public void UpdateSchedChildDepartures(string SchedChildDeparturesID, string finalgift, string finalletter)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.updateSchedChildDepartures(SchedChildDeparturesID, finalgift, finalletter);
        }

        public void RunCheckBot()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.FindCheckbotRecords();
        }

        public void Run8x8Process()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.DownloadAll8x8AudioFile();
        }

        public void RunSafeGaurding()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.FetchCPActivityVolunteers();
        }

        public string SendSMS(string message, string phoneNo)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.SendSMS(message, phoneNo);
        }

        public void createWelcomeCallEnquiry()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.CreateWelcomeJourneyEnqueryOperation();
        }

        public string GetInstagramUserFeed()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.GetInstagramUserFeed();
        }

        public void CreateRGBatch(InitialRGData Data)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.DoCreateBatchRB(Data);
            
        }

        public void DoWork()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();

            obj.Fetch8x8AllData("2021-12-16");
            ///obj.SendMultipleNotificationAsync();
            //obj.GetArrearsPaymentMethodNoneOdata();
            //obj.DoArrearsPaymentMethodNoneProcess();
            //obj.SendLetterNotificationMain();
        }

        public void SendLetterNotificationMain(string env)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.CheckSupporterHasValidEmail(274959);
            //obj.DoArrearsPaymentMethodNoneProcess();
            obj.SendLetterNotificationMain(env);
        }

        public void RunArrearsCancelledDDsProcess()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.RunBehindOnPaymentProcess();
        }

        public void InitiateSetupBot()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.InitiateSetupBot();
        }

        public string CreateRGBatch1(InitialRGData dataObj)
        {
            string JsonString = (new JavaScriptSerializer()).Serialize(dataObj);
            //appLogger.Info("RG Batch data received in Rest Service : " + JsonString);
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //ResponseModel resObj = new ResponseModel();
            dataObj.actionid = 182;// MessageType.CREATE_RG_BATCH;
            string bbrequest = (new JavaScriptSerializer()).Serialize(dataObj);
            SendDataToMainWS1(dataObj);
            //resObj.status = 1;
            //resObj.message = "success";
            return null;
        }

        public bool CheckSponsroshipStartToday(string SponsorshipId)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CheckSponsroshipStartToday(SponsorshipId);
        } 


        public string GetInstagramData()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.GetInstagramData();
        }

        public long CreateIncidentForPhotoRequest(TcptR4DataOperations.Model.SupporterDataModel supporterData)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CreateIncidentForPhotoRequest(supporterData);
        }

        public long CreateIncidentForCompassionTogether(TcptR4DataOperations.Model.CompassionTogether compassiontogether)
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            return obj.CreateIncidentForCompassionTogetherform(compassiontogether);
        }

        public void RunPaymentMethodNoneProcess()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.DoArrearsPaymentMethodNoneProcessOld();
        }

        public void DoDebugWork()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            //obj.SendMultipleNotificationAsync();
            string[] lookupids = new string[] { "8-10072149" };
            foreach (string lookup in lookupids)
            {
                obj.DeleteConstituentProspectPlanInteractionDocumentationList(lookup);
            }
           
            //obj.GetPPNSList("8-10129917");
        }

        public void SendGiftNotification()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.SendGiftNotification();
        }
        
        public void SendLetterUpdateNotification()
        {
            TcptR4DataOperations.TcptR4Operation obj = new TcptR4DataOperations.TcptR4Operation();
            obj.SendLetterUpdateNotification();
        }
    }
}
