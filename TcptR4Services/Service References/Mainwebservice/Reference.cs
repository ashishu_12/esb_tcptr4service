﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TcptR4Services.Mainwebservice {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Mainwebservice.IMainService")]
    public interface IMainService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMainService/IAmOneWayMessagingPattern", ReplyAction="http://tempuri.org/IMainService/IAmOneWayMessagingPatternResponse")]
        void IAmOneWayMessagingPattern(string jsonpayload, int action);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMainService/IAmOneWayMessagingPattern", ReplyAction="http://tempuri.org/IMainService/IAmOneWayMessagingPatternResponse")]
        System.Threading.Tasks.Task IAmOneWayMessagingPatternAsync(string jsonpayload, int action);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMainService/IAmFullDuplexMessagingPattern", ReplyAction="http://tempuri.org/IMainService/IAmFullDuplexMessagingPatternResponse")]
        string IAmFullDuplexMessagingPattern(string JsonString, int action);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMainService/IAmFullDuplexMessagingPattern", ReplyAction="http://tempuri.org/IMainService/IAmFullDuplexMessagingPatternResponse")]
        System.Threading.Tasks.Task<string> IAmFullDuplexMessagingPatternAsync(string JsonString, int action);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMainServiceChannel : TcptR4Services.Mainwebservice.IMainService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MainServiceClient : System.ServiceModel.ClientBase<TcptR4Services.Mainwebservice.IMainService>, TcptR4Services.Mainwebservice.IMainService {
        
        public MainServiceClient() {
        }
        
        public MainServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MainServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MainServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MainServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void IAmOneWayMessagingPattern(string jsonpayload, int action) {
            base.Channel.IAmOneWayMessagingPattern(jsonpayload, action);
        }
        
        public System.Threading.Tasks.Task IAmOneWayMessagingPatternAsync(string jsonpayload, int action) {
            return base.Channel.IAmOneWayMessagingPatternAsync(jsonpayload, action);
        }
        
        public string IAmFullDuplexMessagingPattern(string JsonString, int action) {
            return base.Channel.IAmFullDuplexMessagingPattern(JsonString, action);
        }
        
        public System.Threading.Tasks.Task<string> IAmFullDuplexMessagingPatternAsync(string JsonString, int action) {
            return base.Channel.IAmFullDuplexMessagingPatternAsync(JsonString, action);
        }
    }
}
