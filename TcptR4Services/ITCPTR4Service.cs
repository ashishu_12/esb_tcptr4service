﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TcptR4DataOperations.Model;

namespace TcptR4Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITCPTR4Service" in both code and config file together.
    [ServiceContract]
    public interface ITCPTR4Service
    {
        [OperationContract]
        TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchByParamsDirect(Model.BeneficiaryRequestModel beneficiarySearchchildobj);
        [OperationContract]
        List<TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNResponse> BeneficiaryReservationsFromRN(TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNModel icpSearchObj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchAndHold(Model.BeneficiaryRequestModel beneficiarySearchchildobj, TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiarySearch.Response beneficiarySearchSEF(Model.BeneficiaryRequestModel beneficiarySearchchildobj, string primaryOwner, string secondaryOwner, TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj);

        /*[OperationContract]
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldBeneficiary(BeneficiaryHoldRequestModel childobj);*/

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldBeneficiaries(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childobj);

        [OperationContract]
        void ReleaseBeneficiariesHold(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse HoldAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj);

        [OperationContract]
        //long ReleaseAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj, TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj);
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse ReleaseAndImportBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj, TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childimportobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse ReleaseBeneficiaries(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse UpdateBenHoldSEF(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj, string ChildRNID);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryHold.BeneficiaryHoldResponse UpdateMultippleBenHold(TcptR4DataOperations.Model.BeneficiaryHold.Beneficiaryholdrequestlist[] childholdobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryReservation.Response createBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReservation.Globalpartnerreservationrequestlist[] benReserveRequest, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childreserveobj);

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryReservation.Response updateBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReservation.Globalpartnerreservationrequestlist[] benReserveRequest, TcptR4DataOperations.Model.AddChild.AddChildRequest[] childreserveobj);

        [OperationContract]
        TcptR4DataOperations.Model.DemandPlanning.Response demandPlanning(string base64data);

        [OperationContract]
        void AddChild(TcptR4DataOperations.Model.AddChild.AddChildRequest[] childobj);

        [OperationContract]
        TcptR4DataOperations.Model.ICPSearch.Response icpSearchByParamsDirect(TcptR4DataOperations.Model.ICPSearch.IcpSearchRequestModel icpSearchobj);

        [OperationContract]
        List<TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNResponse> ICPReservationsFromRN(TcptR4DataOperations.Model.ICPSearch.ICPReservationSearchFromRNModel icpSearchObj);

        [OperationContract]
        TcptR4DataOperations.Model.ChurchPartnerReservation.Response icpReservation(TcptR4DataOperations.Model.ChurchPartnerReservation.Globalpartnerreservationrequestlist[] icpReserveReqObj, TcptR4DataOperations.Model.AddICP.AddICPRequest[] importIcpObj);

        [OperationContract]
        TcptR4DataOperations.Model.ICPReleaseReservation.ICPReservationReleaseResponse ReleaseICPReservation(TcptR4DataOperations.Model.ICPReleaseReservation.ReservationCancelRequestList[] cancelReservationObj);

        //     [OperationContract]
        //    List<TcptR4DataOperations.Model.ChildDataModel> specificBeneficiarySearch(string needKey, string globalId ,string nomoney, string sponsorshipAct);

        // test specific search method
        [OperationContract]
        List<TcptR4DataOperations.Model.ChildDataModel> specificBeneficiarySearch(TcptR4DataOperations.Model.SpecificBeneficiarySearchModel searchObj);

        [OperationContract]
        List<TcptR4DataOperations.Model.AddChild.BeneficiaryDataModel> GetUKHoldedChild(TcptR4DataOperations.Model.AddChild.BeneficiaryDataModel childobj);

        //      [OperationContract]
        //      TcptR4DataOperations.Model.BeneficiaryReservation.ReservedChildResponse GetReservedChild(TcptR4DataOperations.Model.BeneficiaryReservation.ReservedChildRequest reservedchildobj);

        [OperationContract]
        TcptR4DataOperations.Model.SupporterGift.GiftCreateResponse SupporterGiftsUploader(string base64data,string giftType);

        [OperationContract]
        List<TcptR4DataOperations.Model.Resubmission.CorrespondenceModel> FetchCorrespondences(string type);

        [OperationContract]
        string ResubmitCorrespondeces(TcptR4DataOperations.Model.Resubmission.CorrespondenceModel data);

        [OperationContract]
        List<TcptR4DataOperations.Model.Resubmission.SupporterGroupModel> FetchSupporterGroups(string type);

        [OperationContract]
        string ResubmitSupporter(string type, TcptR4DataOperations.Model.Resubmission.SupporterGroupModel data);

        [OperationContract]
        List<TcptR4DataOperations.Model.Resubmission.SponsorshipModel> FetchSponsorships(string type);

        [OperationContract]
        string ResubmitSponsorship(string type, TcptR4DataOperations.Model.Resubmission.SponsorshipModel data);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList createInterventionHoldInGMC(TcptR4DataOperations.Model.Intervention.Interventionholdlist interventionObj);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterverntionQueryResponse SearchInterventions(string interventionId);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterventionHoldResponseList updateInterventionHold(TcptR4DataOperations.Model.Intervention.Interventionholdlist interventionObj);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterventionHoldCancelResponse cancelInterventionHold(TcptR4DataOperations.Model.Intervention.Interventionholdcancellationlist interventionObj);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterventionCommitmentResponse createInterventionCommitment(TcptR4DataOperations.Model.Intervention.Interventioncommitmentlist intCommObj);

        [OperationContract]
        List<TcptR4DataOperations.Model.Intervention.InterventionHoldList> getInterventionHoldsFromRN(TcptR4DataOperations.Model.Intervention.InterventionHoldList intObj);

        [OperationContract]
        TcptR4DataOperations.Model.Intervention.InterventionHoldList searchInterventionHoldById(int InterventionRNID);

        [OperationContract]
        void updatecorrespondence();

        [OperationContract]
        TcptR4DataOperations.Model.BeneficiaryReleaseHold.BeneficiaryHoldReleaseResponse CancelBeneficiaryHoldAdminFunction(TcptR4DataOperations.Model.BeneficiaryReleaseHold.Beneficiaryholdreleaserequestlist[] childreleaseobj);

        [OperationContract]
        void testMethod();

        [OperationContract]
        string updateWebToken();

        [OperationContract]
        int SupporterCountToUpdateWebToken();

        [OperationContract]
        int getBeneficiaryCountforAdhocDataRefresh();

        [OperationContract]
        string initiateBeneficiaryAdHocdatarefreshProcess();

        [OperationContract]
        int getDPCount();

        [OperationContract]
        string doDataProtectionProcess();

        [OperationContract]
        void deleteConnectforDataProtection();

        [OperationContract]
        List<TcptR4DataOperations.Model.SupporterDataModel> FetchSupporters(string type);

        [OperationContract]
        string ResubmitSupporters(TcptR4DataOperations.Model.SupporterDataModel data1, string type);

        [OperationContract]
        void getdataDone();

        [OperationContract]
        bool doBstandingOrder(string base64data,string beforedays,string afterdays,string filename);

        [OperationContract]
        bool sendSingleRecordForBarclysStandingOrderProcess(TcptR4DataOperations.Model.BarclaysStandingOrderModel bbobj);

        [OperationContract]
        bool ScrutinizeGMCOperation();

        [OperationContract]
        void DoWhatEverYouWant();

        [OperationContract]
         TcptR4DataOperations.Model.BeneficiaryReleaseReservation.BenReservationReleaseResponse CancelBeneficiaryReservation(TcptR4DataOperations.Model.BeneficiaryReleaseReservation.ReservationCancelRequestList[] childreleaseobj);
        [OperationContract]
        bool doBacsProcess(string base64data);

        [OperationContract]
        void SendNotifications(TcptR4DataOperations.Model.NotificationsModel notificationObjs);

        [OperationContract]
        bool doDDRProcess(string base64data,string date);

        [OperationContract]
        TcptR4DataOperations.Model.ConstituentRevenueAndGiftAidTotalAmount getGiftaidtotalamountandrevenueperyear(string BBID);

        [OperationContract]
        TcptR4DataOperations.Model.ICPDetailModel getICPDetails(string ICPDetails);

        [OperationContract]
        bool DeleteCommitment(string CommitmentID);

        [OperationContract]
         bool createIncidentForAdobeSignatureBeforeSign(string email, string url_for_Travellers, string docket_url_for_Travellers, string url_for_Terms_and_Conditions, string docket_url_for_Terms_and_Conditions);

        [OperationContract]
        void createIncidentForAdobeSignatureAfterSign(string email, string url);

        [OperationContract]
        bool doCAFProcess(string base64data, string beforedays, string afterdays, string filename);

        [OperationContract]
        bool doSEFProcess(string base64data, string Type);

        [OperationContract]
        void UploadMultipart(byte[] file, string filename, string contentType, string url);

        [OperationContract]
        void Createincidentwithattachment(TcptR4DataOperations.Model.createIncidentModel crobj);

        [OperationContract]
        List<TcptR4DataOperations.Model.ChildDataModel> specificBeneficiarySearchNew(TcptR4DataOperations.Model.SpecificBeneficiarySearchModel searchObj);

        [OperationContract]
        bool doRateChangeProcess(string base64data, string identifier);

        [OperationContract]
        List<TcptR4DataOperations.Model.DonationModel> getConstituentDonationTableDetails(string lookupid);

        [OperationContract]
        void MajorDonorProcessInDWH();

        [OperationContract]
        bool doStewardshipProcess(string base64data,string filename);

        [OperationContract]
        List<TcptR4DataOperations.Model.SupporterCommitmentModel> GetCommitmentBySupporterGroup(string supGroupId);

        [OperationContract]
        List<TcptR4DataOperations.Model.cancelRecurringGift> cancelRecurringGift(List<TcptR4DataOperations.Model.cancelRecurringGift> rgobj, string delinkReason);

        [OperationContract]
        void CreateIncidentForcancelRecurringGift(List<TcptR4DataOperations.Model.cancelRecurringGift> rgobj, string delinkReason, int AssignedAccount);

        [OperationContract]
        void CreateCancelRGIncidentWithSummary(List<TcptR4DataOperations.Model.RGDetails> CreateRG , List<TcptR4DataOperations.Model.RGDetails> CancelRG);

        [OperationContract]
        List<TcptR4DataOperations.Model.RecurringGiftModel> FetchRGDetailsOnLookupID(string lookupid, string GroupType, string SupporterGroupID);


        [OperationContract]
        void getdatadone();

        [OperationContract]
        bool doGDPRprocess(string base64data, int ScopeId);

        [OperationContract]
        void SendNotification(string Supporterid, string NeedKey,string Env);

        [OperationContract]
        long CreateIncidentForCoffee(TcptR4DataOperations.Model.SupporterDataModel supporterData);

        [OperationContract]
        long CreateIncidentForAmbassadors(TcptR4DataOperations.Model.AmbassadorsModal ambassadorsdata);

        [OperationContract]
        long CreateIncidentForSupPreferredName(TcptR4DataOperations.Model.SupporterDataModel supporterData);

        [OperationContract]
        void UpdateSchedChildDepartures(string SchedChildDeparturesID, string finalgift, string finalletter);

        [OperationContract]
        void RunCheckBot();

        [OperationContract]
        void Run8x8Process();

        [OperationContract]
        void RunSafeGaurding();

        [OperationContract]
        string SendSMS(string message, string phoneNo);

        [OperationContract]
        void createWelcomeCallEnquiry();

        [OperationContract]
        string GetInstagramUserFeed();

        [OperationContract]
        void CreateRGBatch(TcptR4DataOperations.Model.InitialRGData Data);

        [OperationContract]
        List<TcptR4DataOperations.Model.RecurringGiftModel> GetOtherRGOnLookupID(string lookupid);

        [OperationContract]
        void DoWork();

        [OperationContract]
        void SendLetterNotificationMain(string env);

        [OperationContract]
        void RunArrearsCancelledDDsProcess();

        [OperationContract]
        void InitiateSetupBot();

        [OperationContract]
        string CreateRGBatch1(InitialRGData Data);

        [OperationContract]
        bool CheckSponsroshipStartToday(string SponsorshipId);

        [OperationContract]
        string GetInstagramData();

        [OperationContract]
        long CreateIncidentForPhotoRequest(TcptR4DataOperations.Model.SupporterDataModel supporterData);

        [OperationContract]
        long ReportGenerator();

        [OperationContract]
        long CreateIncidentForCompassionTogether(TcptR4DataOperations.Model.CompassionTogether compassiontogether);

        [OperationContract]
        void RunPaymentMethodNoneProcess();

        [OperationContract]
        void DoDebugWork();

        [OperationContract]
        void SendGiftNotification();

        [OperationContract]
        void SendLetterUpdateNotification();

    }
}
