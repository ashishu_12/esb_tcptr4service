﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TcptR4Services
{
    [DataContract]
    public class BeneficiaryHoldRequestModel
    {
        [DataMember]
        public string BeneficiaryState { get; set; }
        [DataMember]
        public object EstimatedNoMoneyYieldRate { get; set; }
        [DataMember]
        public DateTime HoldEndDate { get; set; }
        [DataMember]
        public object HoldID { get; set; }
        [DataMember]
        public object HoldYieldRate { get; set; }
        [DataMember]
        public bool IsSpecialHandling { get; set; }
        [DataMember]
        public string PrimaryHoldOwner { get; set; }
        [DataMember]
        public object SecondaryHoldOwner { get; set; }
        [DataMember]
        public object SourceCode { get; set; }
        [DataMember]
        public string Beneficiary_GlobalID { get; set; }
        [DataMember]
        public object Channel_Name { get; set; }
        [DataMember]
        public string GlobalPartner_ID { get; set; }
    }
}