﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TcptR4Services.Model
{
    public class SendDatatoEsbModel
    {
        public int  actionid { get; set; }
        public string globalid { get; set; }
        public string icpid { get; set; }
        public string Supporterid { get; set; }
        public string isAdHocDataRefreshPacket { get; set; }
    }
}