﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TcptR4Services.Model
{
    public class BeneficiaryRequestModel
    {
        public string LongWaiting { get; set; }
        public string localId { get; set; }
        public string gender { get; set; }
        public string countries { get; set; }
        public string[] multipleCountries { get; set; }
        public string minPriorityScore { get; set; }
        public string maxPriorityScore { get; set; }
        public string name { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string icpId { get; set; }
        public string birthDateFrom { get; set; }
        public string birthDateTo { get; set; }
        public string birthDay{ get; set; }
        public string birthMonth { get; set; }
        public string birthYear { get; set; }
        public string minDaysWaiting { get; set; }
        public string maxDaysWaiting { get; set; }
        public string includeOnHold { get; set; }
        public string status { get; set; }
        public string[] statuses { get; set; }
        public string hasSpecialNeeds { get; set; }
        public string hasSiblings { get; set; }
        public string isFatherAlive { get; set; }
        public string isMotherAlive { get; set; }
        public string cluster { get; set; }
        public string isPhysicalDisable { get; set; }
        public string IsOrphan { get; set; }
        public string hivAffectedArea { get; set; }
        public string chronicIllness { get; set; }
        public string isTransitioned { get; set; }
        public string globalId { get; set; }
        public int take { get; set; }
        public int skip { get; set; }
        public string age { get; set; }
        public string lowage { get; set; }
        public string highage { get; set; }
        public string[] territory { get; set; }
        public string BeneficiaryLastReviewDate { get; set; }
        public string SourceCode { get; set; }
        public string[] HoldingGlobalPartnerID { get; set; }
        

    }
}