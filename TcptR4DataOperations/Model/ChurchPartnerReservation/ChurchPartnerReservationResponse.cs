﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ChurchPartnerReservation
{
    public class Response
    {
        public Globalpartnerreservationresponselist[] GlobalPartnerReservationResponseList { get; set; }
        public int Status { get; set; }
        public Error Error { get; set; }
    }

    public class Globalpartnerreservationresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string ICP_ID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string Reservation_ID { get; set; }
    }
}