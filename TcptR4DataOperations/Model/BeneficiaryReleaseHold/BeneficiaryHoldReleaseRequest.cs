﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryReleaseHold
{
    public class BeneficiaryHoldReleaseRequest
    {
        public Beneficiaryholdreleaserequestlist[] BeneficiaryHoldReleaseRequestList { get; set; }
    }

    [DataContract]
    public class Beneficiaryholdreleaserequestlist
    {
        [DataMember]
        public string Beneficiary_GlobalID { get; set; }
        [DataMember]
        public string GlobalPartner_ID { get; set; }
        [DataMember]
        public string HoldID { get; set; }
        [DataMember]
        public string ReservationId { get; set; }
    }

}
