﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryReleaseHold
{
    public class BeneficiaryHoldReleaseResponse
    {
        public int Status { get; set; }
        public Beneficiaryholdreleaseresponselist[] BeneficiaryHoldReleaseResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiaryholdreleaseresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string HoldID { get; set; }
        public string ReservationId { get; set; }

    }
}
