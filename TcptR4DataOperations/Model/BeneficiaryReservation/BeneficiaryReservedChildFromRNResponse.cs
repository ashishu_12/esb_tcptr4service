﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryReservation
{ 
    public class ReservedChildResponse
    {
        public ReservedChildResponselist[] ReservedChildResponseList { get; set; }
    }

    public class ReservedChildResponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string Channel_Name { get; set; }
      //  public string GlobalPartner_ID { get; set; }
        public string ICP_ID { get; set; }
 //       public string CampaignEventIdentifier { get; set; }
        public string ExpirationDate { get; set; }
        public string HoldExpirationDate { get; set; }
     //   public int HoldYieldRate { get; set; }
     //   public string ID { get; set; }
        public bool IsReservationAutoApproved { get; set; }
     //   public string NumberOfBeneficiaries { get; set; }
        public string PrimaryOwner { get; set; }
        public int ReservationType { get; set; }
        public int ReservationStatus { get; set; }
        public int Need { get; set; }
        //   public string SecondaryOwner { get; set; }
        //   public string SourceCode { get; set; }

        //    public string Reservation_ID { get; set; }
        //    public string Message { get; set; }
        //    public string Code { get; set; }
    }
}
