﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class CorrespondentResponse
    {
        public int Status { get; set; }
        public Correspondentcreateresponselist[] CorrespondentCreateResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Correspondentcreateresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string CorrespondentCommitmentID { get; set; }
        public string StartDate { get; set; }
    }
}
