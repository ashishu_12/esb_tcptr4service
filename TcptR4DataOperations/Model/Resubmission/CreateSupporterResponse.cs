﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class CreateSupporterResponse
    {
        public Supporterprofileresponse[] SupporterProfileResponse { get; set; }
        public Error Error { get; set; }
    }

    public class Supporterprofileresponse
    {
        public string GlobalID { get; set; }
        public string GPID { get; set; }

    }
}