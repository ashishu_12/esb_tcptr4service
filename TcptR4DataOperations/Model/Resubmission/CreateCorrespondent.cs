﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class CreateCorrespondent
    {
        public Correspondentlist[] CorrespondentList { get; set; }
    }

    public class Correspondentlist
    {
        public string CorrespondenceEndDate { get; set; }
        public string CorrespondentCorrespondenceLanguage { get; set; }
        public string CorrespondentGlobalPartnerID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SponsorGlobalPartnerID { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string Channel_Name { get; set; }

    }
}
