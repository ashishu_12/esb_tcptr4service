﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class CancellationListResponse
    {
        public int Status { get; set; }
        public Beneficiarycommitmentcancellationlistresponse[] BeneficiaryCommitmentCancellationListResponse { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiarycommitmentcancellationlistresponse
    {
        public string Beneficiary_GlobalID { get; set; }
        public string SponsorSupporterGlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string EndDate { get; set; }
        public string HoldID { get; set; }
        public string Commitment_ID { get; set; }
    }
}
