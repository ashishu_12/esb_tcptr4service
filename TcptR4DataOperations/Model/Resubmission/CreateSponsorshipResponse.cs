﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Resubmission
{
    public class SponsorshipResponse
    {
        public int Status { get; set; }
        public Beneficiarycommitmentresponselist[] BeneficiaryCommitmentResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiarycommitmentresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string SponsorSupporterGlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string Commitment_ID { get; set; }
        public string CorrespondentCommitmentID { get; set; }
        public string HoldID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public string StartDate { get; set; }
    }
}
