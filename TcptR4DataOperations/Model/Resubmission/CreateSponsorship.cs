﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class Sponsorship
    {
        public object CommitmentEndDate { get; set; }
        public object CompassNeedKey { get; set; }
        public object CorrespondenceEndDate { get; set; }
        public object CorrespondentCorrespondenceLanguage { get; set; }
        public object CorrespondentGlobalPartnerID { get; set; }
        public object CorrespondentSupporterGlobalID { get; set; }
        public object FinalCommitmentOfLine { get; set; }
        public string HoldID { get; set; }
        public bool IsSponsorCorrespondent { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SponsorCorrespondenceLanguage { get; set; }
        public string SponsorGlobalPartnerID { get; set; }
        public string SponsorSupporterGlobalID { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public object Channel_Name { get; set; }
        public object LinkType { get; set; }
        public object ParentCommitmentID { get; set; }
    }
}
