﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class SupporterGroupModel
    {
        public long Id { get; set; }
        public long ConId { get; set; }
        public string Name { get; set; }
        public string GlobalId { get; set; }
        public string Status { get; set; }
        public string LetterSalutation { get; set; }
    }
}
