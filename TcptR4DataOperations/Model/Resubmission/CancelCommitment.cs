﻿using System;

namespace TcptR4DataOperations.Model.Resubmission
{
    public class CancelCommitmentModel
    {
        public Beneficiarycommitmentcancellationlist[] BeneficiaryCommitmentCancellationList { get; set; }
    }

    public class Beneficiarycommitmentcancellationlist
    {
        public object FinalCommitmentOfLine { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public DateTime HoldExpirationDate { get; set; }
        public string Commitment_ID { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SponsorSupporterGlobalID { get; set; }
        public object DelinkType { get; set; }
        public string GlobalPartner_ID { get; set; }
        public string HoldType { get; set; }
    }
}
