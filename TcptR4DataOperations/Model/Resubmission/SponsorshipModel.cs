﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class SponsorshipModel
    {
        public long Id { get; set; }
        public long SGId { get; set; }
        public long ConId { get; set; }
        public string SGName { get; set; }
        public string SGGlobalId { get; set; }
        public long NeedId { get; set; }
        public string BenGlobalId { get; set; }
        public string BenLocalId { get; set; }
        public long BenOrIcpHoldId { get; set; }
        public long LinkTypeId { get; set; }
        public long SponsorshipType { get; set; }
        public string BenHoldId { get; set; }
        public string GlobalId { get; set; }
    }
}
