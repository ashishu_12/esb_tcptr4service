﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Resubmission
{
    public class CorrespondenceModel
    {
        public long Id { get; set; }
        public string LetterMessage { get; set; }
        public string LetterFrom { get; set; }
        public string LetterTo { get; set; }
        public string ReceivedDate { get; set; }
        public string CardMessage { get; set; }
        public string CardFrom { get; set; }
        public string CardTo { get; set; }
        public string CancelLetter { get; set; }
        public string CancelCard { get; set; }
        public string SupporterGroup { get; set; }
        public long Need { get; set; }
        public string CommKitID { get; set; }
        public string FinalLetterURL { get; set; }
    }
}
