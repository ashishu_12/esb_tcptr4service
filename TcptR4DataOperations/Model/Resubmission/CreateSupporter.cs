﻿namespace TcptR4DataOperations.Model.Resubmission
{
    public class SupporterData
    {
        public Supporterprofile[] SupporterProfile { get; set; }
    }

    public class Supporterprofile
    {
        public string GlobalPartner { get; set; }
        public string CorrespondenceDeliveryPreference { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string GPID { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public bool MandatoryReview { get; set; }
        public string GlobalId { get; set; }
        public string compassConId { get; set; }
    }
}


