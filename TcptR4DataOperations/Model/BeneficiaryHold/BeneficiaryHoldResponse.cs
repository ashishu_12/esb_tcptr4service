﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryHold
{
    public class BeneficiaryHoldResponse
    {
        public int Status { get; set; }
        public Beneficiaryholdresponselist[] BeneficiaryHoldResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiaryholdresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string Beneficiary_LocallID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string HoldID { get; set; }
        public long ChildRNID { get; set; }
    }
}
