﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryHold
{
    public class BeneficiaryUpdateHoldResponse
    {
        public int Status { get; set; }
        public Beneficiaryupdateholdresponselist[] BeneficiaryUpdateHoldResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiaryupdateholdresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string HoldID { get; set; }
    }
}
