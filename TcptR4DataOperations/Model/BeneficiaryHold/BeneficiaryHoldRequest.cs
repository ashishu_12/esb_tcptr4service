﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryHold
{
    public class BeneficiaryHoldRequest
    {
        public Beneficiaryholdrequestlist[] BeneficiaryHoldRequestList { get; set; }
    }

    [DataContract]
    public class Beneficiaryholdrequestlist
    {
        [DataMember]
        public string BeneficiaryState { get; set; }
        [DataMember]
        public object EstimatedNoMoneyYieldRate { get; set; }
        [DataMember]
        public DateTime HoldEndDate { get; set; }
        [DataMember]
        public string HoldID { get; set; }
        [DataMember]
        public string HoldYieldRate { get; set; }
        [DataMember]
        public bool IsSpecialHandling { get; set; }
        [DataMember]
        public string PrimaryHoldOwner { get; set; }
        [DataMember]
        public string SecondaryHoldOwner { get; set; }
        [DataMember]
        public string SourceCode { get; set; }
        [DataMember]
        public string Beneficiary_GlobalID { get; set; }
        [DataMember]
        public string Beneficiary_LocalID { get; set; }
        [DataMember]
        public string WaitingSinceDate { get; set; }
        //[DataMember]
        //public string Code { get; set; }
        [DataMember]
        public string Channel_Name { get; set; }
        [DataMember]
        public string ChannelId { get; set; }
        [DataMember]
        public string GlobalPartner_ID { get; set; }
        [DataMember]
        public string HoldStatus { get; set; }
    }

}
