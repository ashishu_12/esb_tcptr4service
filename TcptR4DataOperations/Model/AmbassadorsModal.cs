﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class AmbassadorsModal
    {
         public string firstname { get; set; }
         public string surname { get; set; }
         public string interest { get; set; }
         public string email { get; set; }
         public string SupId { get; set; }
         public string phonenumber { get; set; }
         public string Source { get; set; }
        public string postcode { get; set; }   
    }
}
