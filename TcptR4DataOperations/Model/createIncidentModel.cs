﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class createIncidentModel
    {
        public int actionid { get; set; }
        public long supporterId { get; set; }
        public string ChildKey { get; set; }
        public string image1base64 { get; set; }
        public string image2base64 { get; set; }
        //public string subject { get; set; }
        public string applicationCode { get; set; }
        //public string sponsorship { get; set; }
        public string ChildNameLite { get; set; }
        public string OriginatingEvent { get; set; }
        public string ApplicationCode { get; set; }
        public string CreatedByAccount { get; set; }
        public string response { get; set; }
        public long incidentId { get; set; }
        public string office { get; set; }
        public string image1path { get; set; }
        public string image2path { get; set; }
    }
}
