﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class GDPRDynamicModel
    {
        public string System { get; set; }
        public string ObjectType { get; set; }
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        
    }

    public class GDPRCsv
    {
        public long ContactID { get; set; }
        public long SupporterID { get; set; }
        public long SGID { get; set; }
        public long GlobalID { get; set; }
        public string BBID { get; set; }
        public string RequesType { get; set; }
        public string RuleID { get; set; }
        public long Recordid { get; set; }
        public long ResourceOrder { get; set; }
    }

    public class ProcessScheduleManager
    {
        public string ID { get; set; }
        public string ProcessName { get; set; }
        public string LastSuccessfulRunTime { get; set; }
        public string Status { get; set; }
    }
}
