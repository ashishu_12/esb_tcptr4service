﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class SupGrpResponseModel
    {
        public long SupGrpId { get; set; }
        public string GlobalId { get; set; }
    }
    public class PreferredNameChange
    {
        public long SupporterGroupID { get; set; }
        public string NewValue { get; set; }
        public string PrevValue { get; set; }
        public string ReasonForChange { get; set; }
        public int Source { get; set; }
        public string Context { get; set; }
        public long Supporter { get; set; }
        public int Account { get; set; }
    }
}
