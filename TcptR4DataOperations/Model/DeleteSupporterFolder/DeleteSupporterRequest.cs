﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TcptR4DataOperations.Model
{
    public class DeleteSupporterRequest
    {
        public DeleteSupporterDatarequestlist[] DeleteSupporterDataRequestList { get; set; }
    }

    [DataContract]
    public class DeleteSupporterDatarequestlist
    {
        [DataMember]
        public string RequestID { get; set; }
        [DataMember]
        public string GP_ID { get; set; }
        [DataMember]
        public string GP_SupporterID { get; set; }
        [DataMember]
        public string GlobalSupporterID { get; set; }
        [DataMember]
        public string DataProtection_Type { get; set; }
    }

}
