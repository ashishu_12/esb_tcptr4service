﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.DemandPlanning
{
    public class Request
    {
        public Globalpartnerweeklydemandrequest GlobalPartnerWeeklyDemandRequest { get; set; }
    }
    public class Globalpartnerweeklydemandrequest
    {
        public Globalpartnerweeklydemandrequestlist[] GlobalPartnerWeeklyDemandRequestList { get; set; }
        public string GlobalPartner_ID { get; set; }
    }
    public class Globalpartnerweeklydemandrequestlist
    {
        public int ResupplyQuantity { get; set; }
        public int TotalDemand { get; set; }
        public string WeekStartDate { get; set; }
        public string WeekEndDate { get; set; }
        public string errorOutput { get; set; }
    }
}
