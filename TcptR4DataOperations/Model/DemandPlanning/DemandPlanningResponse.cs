﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.DemandPlanning
{
    public class Response
    {
        public int Status { get; set; }
        public Globalpartnerweeklydemandresponsedetail[] GlobalPartnerWeeklyDemandResponseDetails { get; set; }
        public Error Error { get; set; }
        public string ErrorOutput { get; set; }
    }

    public class Globalpartnerweeklydemandresponsedetail
    {
        public int Code { get; set; }
        public string Message { get; set; } 
    }
}
