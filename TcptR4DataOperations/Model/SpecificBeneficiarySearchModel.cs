﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class SpecificBeneficiarySearchModel
    {
        public string Needkey { get; set; }
        public string globalId { get; set; }
        public string nomoney { get; set; }
        public string sponsorshipAct { get; set; }
        public bool isSpecificChildPool { get; set; }
        public string isDepsub { get; set; }
        public string isSub { get; set; }
        public string isCorrespondence { get; set; }
    }
}
