﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class RateChange
    {
        public int ActionID  { get;set; }
        public string RecordID { get; set; }
        public string ConstituentBBID { get; set; }
        public string BBIDConstituentType { get; set; }
        public string MandateReference { get; set; }
        public string Frequency { get; set; }
        public string RateChangeCategory { get; set; }
        public string RateChangedDate { get; set; }
        public string Amount { get; set; }
        public string NextPaymentDate { get; set; }
        public string DatetoSendEmail { get; set; }
        public int SupporterID { get; set; }
        public string mergeString { get; set; }

    }
}
