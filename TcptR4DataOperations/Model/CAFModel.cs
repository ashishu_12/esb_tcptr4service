﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class CAFModel
    {
        public int actionid { get; set; }
        public long recordid { get; set; }
        public string BBID { get; set; }
        public string DonorName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string TownCity { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string Anonymous { get; set; }
        public string DonorURN { get; set; }
        public string MarketingPreference { get; set; }
        public string DonationReference { get; set; }
        public string DonationAmount { get; set; }
        public string DonationType { get; set; }
        public string Charge { get; set; }
        public string ChargeType { get; set; }
        public string NetAmount { get; set; }
        public string SpecialInstruction { get; set; }
        public string DonationMethod { get; set; }
        public string DonorGAD { get; set; }
        public string DonationTypeReference { get; set; }
        public string DonationStatus { get; set; }
        public string CampaignCode { get; set; }
        public string Source { get; set; }
        public string GiftAidAmount { get; set; }
        public string GiftAidStatus { get; set; }
        public string CAFRef { get; set; }
        public string ObjectDateTime { get; set; }
        public string filename { get; set; }
        public string beforedays { get; set; }
        public string afterdays { get; set; }
    }
}
