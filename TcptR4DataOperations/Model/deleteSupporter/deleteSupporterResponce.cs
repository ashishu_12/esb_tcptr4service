﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.deleteSupporter
{

    class DeleteSupporterResponse
    {
        public int Status { get; set; }
        public Deletesupporterresponselist[] DeleteSupporterResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Deletesupporterresponselist
    {
        public string RequestID { get; set; }
        public string GP_ID { get; set; }
        public string GP_SupporterID { get; set; }
        public string GlobalSupporterID { get; set; }
        public string DataProtection_Type { get; set; }
    }
}
