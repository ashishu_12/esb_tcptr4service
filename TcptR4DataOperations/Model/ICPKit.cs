﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{

    public class ICPKit
    {
        public Icpresponselist[] ICPResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Icpresponselist
    {
        public string icpbio { get; set; }
        public string icpbioInHtml { get; set; }
        public string Country_Name { get; set; }
        public int? AirportDistance { get; set; }
        public string AirportPreferredTransportation { get; set; }
        public int? AirportTravelTime { get; set; }
        public object AnnualPrimarySchoolCostLocalCurrency { get; set; }
        public object AnnualSecondarySchoolCostLocalCurrency { get; set; }
        public string AverageCoolestTemperature { get; set; }
        public string AverageWarmestTemperature { get; set; }
        public string Climate { get; set; }
        public string ClosestMajorCityEnglish { get; set; }
        public string CoolestMonth { get; set; }
        public string CulturalRitualsAndCustoms { get; set; }
        public string EconomicNeedDescription { get; set; }
        public string EducationalNeedDescription { get; set; }
        public string FamilyMonthlyIncome { get; set; }
        public object[] HarvestMonths { get; set; }
        public string HomeFloor { get; set; }
        public string HomeRoof { get; set; }
        public string HomeWall { get; set; }
        public object[] HungerMonths { get; set; }
        public string LocaleType { get; set; }
        public object MonthSchoolYearBegins { get; set; }
        public string Community_Name { get; set; }
        public object[] PlantingMonths { get; set; }
        public int? Population { get; set; }
        public string[] PrimaryDiet { get; set; }
        public string PrimaryEthnicGroup { get; set; }
        public string PrimaryLanguage { get; set; }
        public string[] PrimaryOccupation { get; set; }
        public object[] RainyMonths { get; set; }
        public string SocialNeedsDescription { get; set; }
        public string SpiritualNeedDescription { get; set; }
        public string Terrain { get; set; }
        public object TravelTimeToMedicalServices { get; set; }
        public Double? UnemploymentRate { get; set; }
        public string WarmestMonth { get; set; }
        public string Country { get; set; }
        public string Denomination { get; set; }
        public object FieldOffice_Country { get; set; }
        public string FieldOffice_Name { get; set; }
        public object FieldOffice_RegionName { get; set; }
        public string AddressStreet { get; set; }
        public object AllocatedSurvivalSlots { get; set; }
        public bool? AvailableForVisits { get; set; }
        public string ChildDevelopmentCenterName { get; set; }
        public object ChildDevelopmentCenterNameLocalLanguage { get; set; }
        public object[] ChurchMinistry { get; set; }
        public string City { get; set; }
        public string Cluster { get; set; }
        public string[] CognitiveActivities0To5 { get; set; }
        public string[] CognitiveActivities12Plus { get; set; }
        public string[] CognitiveActivities6To11 { get; set; }
        public object[] CommunityInvolvement { get; set; }
        public bool? CompassionConnectEnabled { get; set; }
        public object ComputersForBeneficiaryUse { get; set; }
        public object ComputersForStaffUse { get; set; }
        public string CountryDivision { get; set; }
        public object CurrentStageInQavahProcess { get; set; }
        public object ElectricalPowerAvailability { get; set; }
        public object[] Facilities { get; set; }
        public object FacilityOwnershipStatus { get; set; }
        public string FirstLetterWritingMonth { get; set; }
        public string FirstPartnershipAgreementSignedDate { get; set; }
        public object FloorArea { get; set; }
        public object FoundationDate { get; set; }
        public float? GPSLatitude { get; set; }
        public float? GPSLongitude { get; set; }
        public string HealthContextNeeds { get; set; }
        public int? HomeBasedSponsorshipBeneficiaries { get; set; }
        public string ICPStatus { get; set; }
        public string ICP_ID { get; set; }
        public string[] ImplementedProgram { get; set; }
        public object[] InterestedGlobalPartnerName { get; set; }
        public string InternationalDenominationAffiliation { get; set; }
        public object InternetAccess { get; set; }
        public bool? IsParticipatingInQavahProcess { get; set; }
        public string LastReviewedDate { get; set; }
        public object[] MobileInternetAccess { get; set; }
        public string ICP_Name { get; set; }
        public object ICP_NameNonLatin { get; set; }
        public object NumberOfActiveMembers { get; set; }
        public object NumberOfClassrooms { get; set; }
        public object NumberOfLatrines { get; set; }
        public int? NumberOfSponsorshipBeneficiaries { get; set; }
        public int? NumberOfSurvivalBeneficiaries { get; set; }
        public object OnSiteInternetQuality { get; set; }
        public string[] PhysicalActivities0To5 { get; set; }
        public string[] PhysicalActivities12Plus { get; set; }
        public string[] PhysicalActivities6To11 { get; set; }
        public string PostalCode { get; set; }
        public object PreferredLanguage { get; set; }
        public object ProgramBreakReason { get; set; }
        public object ProgramBreakStartDate { get; set; }
        public object ProgramEndDate { get; set; }
        public string ProgramStartDate { get; set; }
        public string[] ProgramsOfInterest { get; set; }
        public object ProjectActivitiesForFamilies { get; set; }
        public object[] SchoolCostPaidByICP { get; set; }
        public string SecondLetterWritingMonth { get; set; }
        public object SocialMedia { get; set; }
        public string[] SocioEmotionalActivities0To5 { get; set; }
        public string[] SocioEmotionalActivities12Plus { get; set; }
        public string[] SocioEmotionalActivities6To11 { get; set; }
        public string[] SpiritualActivities0To5 { get; set; }
        public string[] SpiritualActivities12Plus { get; set; }
        public string[] SpiritualActivities6To11 { get; set; }
        public string Territory { get; set; }
        public object[] TranslationCompletedFields { get; set; }
        public object[] TranslationRequiredFields { get; set; }
        public string TranslationStatus { get; set; }
        public object[] UtilitiesOnSite { get; set; }
        public object Website { get; set; }
        public object WeeklyChildAttendance { get; set; }
        public string[] MajorRevision_RevisedValues { get; set; }
        public string SourceKitName { get; set; }
    }
}
