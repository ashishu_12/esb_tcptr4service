﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.AddChild
{
    public class ValidateUKBankAccountResponseModel
    {
        public bool valid { get; set; }
        public string invalidParameter { get; set; }
        public string invalidReason { get; set; }
    }
}
