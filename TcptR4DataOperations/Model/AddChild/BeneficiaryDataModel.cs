﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.AddChild
{
   public class BeneficiaryDataModel
    {
        public string Beneficiary_GlobalID { get; set; }
        public string HoldID { get; set; }
        
        public string HoldDateExpiry { get; set; }
        public string HoldType { get; set; }
        public string HoldStatus { get; set; }

        public string PrimaryConatct { get; set; }
        public string SecondaryConatct { get; set; }
        public string SourceCode { get; set; }
        public string NeedId { get; set; }

        //
        public string Country { get; set; }
        public string ICP { get; set; }
        public string IcpName { get; set; }
        public string PriorityScore { get; set; }
        public string StartDate { get; set; }
        public string FullName { get; set; }
    }
}
