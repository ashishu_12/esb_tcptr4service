﻿using System.Runtime.Serialization;

namespace TcptR4DataOperations.Model.AddChild
{
    [DataContract]
    public class AddChildRequest
    {
        [DataMember]
        public string globalId { get; set; }
        [DataMember]
        public string localId { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string icpId { get; set; }
        [DataMember]
        public string holdId { get; set; }
        [DataMember]
        public string fullName { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string preferredName { get; set; }
        [DataMember]
        public string fullbodyimageUrl { get; set; }
        [DataMember]
        public string gender { get; set; }
        [DataMember]
        public string birthDate { get; set; }
        [DataMember]
        public bool orphan { get; set; }
        [DataMember]
        public int needStatus { get; set; }
        [DataMember]
        public bool IsInHIVAffectedArea { get; set; }
        [DataMember]
        public bool IsSpecialNeeds { get; set; }
        [DataMember]
        public string ICP_Name { get; set; }
        [DataMember]
        public string WaitingSinceDate { get; set; }
        [DataMember]
        public string PriorityScore { get; set; }
        [DataMember]
        public string Local_Beneficiary_Number { get; set; }
        [DataMember]
        public string Fo_Name { get; set; }
        [DataMember]
        public int isImportToBB { get; set; }
    }

}
