﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterventionHoldList
    {
        public int ID { get; set; }
        public string InterventionID { get; set; }
        public string InterventionName { get; set; }
        public string InitiationType { get; set; }
        public string HoldID { get; set; }
        public string HoldAmount { get; set; }
        public string HoldStatus { get; set; }
        public bool NextYearOptIn { get; set; }
        public string PrimaryOwner { get; set; }
        public string SecondaryOwner { get; set; }
        public string ServiceLevelAgreement { get; set; }
        public string ExpirationDate { get; set; }

        // GMC data 
        public string TotalPDCCost { get; set; }
        public string TotalInterventionAmount { get; set; }
        public string TotalEstimatedCosts { get; set; }
        public string RemainingAmountToRaise { get; set;}

        // search params
        public string intid { get; set; }
        public string intName { get; set; }
        public int totalRecords { get; set; }
        public int start { get; set; }
        public int records { get; set; }
    }
}
