﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{

    public class InterventionHoldCancelRequest
    {
        public Interventionholdcancellationlist[] InterventionHoldCancellationList { get; set; }
    }

    public class Interventionholdcancellationlist
    {
        public string GlobalPartner_ID { get; set; }
        public string Intervention_HoldID { get; set; }
        public string Intervention_ID { get; set; }
    }
}
