﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterventionCommitmentRequest
    {
        public Interventioncommitmentlist[] InterventionCommitmentList { get; set; }
    }

    public class Interventioncommitmentlist
    {
        public bool? IsRequestedAdditionalFundCommitted { get; set; }
        public string Commitment_Amount { get; set; }
        public string GlobalPartner_ID { get; set; }
        public string HoldID { get; set; }
        public string Intervention_ID { get; set; }
    }

}
