﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterventionHoldCancelResponse
    {
        public int Status { get; set; }
        public Error Error { get; set; }
        public Interventionholdcancellationresponselist[] InterventionHoldCancellationResponseList { get; set; }
    }

    public class Interventionholdcancellationresponselist
    {
        public string GlobalPartner_ID { get; set; }
        public string Intervention_HoldID { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public string Intervention_ID { get; set; }
    }

}
