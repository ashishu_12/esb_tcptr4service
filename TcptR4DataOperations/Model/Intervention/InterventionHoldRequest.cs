﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterventionHoldRequest
    {
        public Interventionholdlist[] InterventionHoldList { get; set; }
    }

    public class Interventionholdlist
    {
        public string GlobalPartner_ID { get; set; }
        public string Intervention_ID { get; set; }
        public string ExpirationDate { get; set; }
        public string HoldAmount { get; set; }
        public string HoldID { get; set; }
        public bool? NextYearOptIn { get; set; }
        public string PrimaryOwner { get; set; }
        public string SecondaryOwner { get; set; }
        public string ServiceLevelAgreement { get; set; }
        public bool? IsRequestedAdditionalFundComm { get; set; }
    }

}
