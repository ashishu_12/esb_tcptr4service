﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterverntionQueryResponse
    {
        public Interventionqueryresponselist[] InterventionQueryResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Interventionqueryresponselist
    {
        public string AdditionalMarketingInformation { get; set; }
        public string Intervention_Name { get; set; }
        public string Intervention_ID { get; set; }
        public string InterventionType { get; set; }
        public string GlobalPartner_ID { get; set; }
        public string RemainingAmountToRaise { get; set; }
        public string FundingStatus { get; set; }
        public string PlannedSponsorshipBeneficiariesNumber { get; set; }
        public string StartNoLaterThanDate { get; set; }
        public string Description { get; set; }
        public string InterventionCategory { get; set; }
        public string InterventionSubCategory_Name { get; set; }
        public string ProposedStartDate { get; set; }
        public string ICP_ID { get; set; }
        public string FieldOffice_ID { get; set; }
        public bool IsFieldOfficePriority { get; set; }
        public string FundCode { get; set; }
        public string TotalInterventionAmount { get; set; }
        public string TotalEstimatedCosts { get; set; }
        public string TotalPDCCost { get; set; }
        public string AdditionalRequestedFunding { get; set; }
        public string ParentInterventionID { get; set; }
    }

}
