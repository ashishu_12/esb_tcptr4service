﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{
    public class InterventionFilterList
    {
        public Interventionquery InterventionQuery { get; set; }
    }

    public class Interventionquery
    {
        public Interventionfilter[] InterventionFilter { get; set; }
    }

    public class Interventionfilter
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public string[] Value { get; set; }
    }

}
