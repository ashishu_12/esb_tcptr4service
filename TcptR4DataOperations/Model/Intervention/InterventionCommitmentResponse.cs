﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.Intervention
{  
    public class InterventionCommitmentResponse
    {
        public int Status { get; set; }
        public Error Error { get; set; }
        public Interventioncommitmentresponselist[] InterventionCommitmentResponseList { get; set; }
    }

    public class Interventioncommitmentresponselist
    {
        public string HoldID { get; set; }
        public string GlobalPartner_ID { get; set; }
        public string Intervention_ID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }

}
