﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class RNReservationDataModel
    {
        public string Beneficiary_GlobalID { get; set; }
        public string CampaignEventIdentifier { get; set; }
        public string Channel_Name { get; set; }
        public string ICP_ID { get; set; }
        public string PrimaryOwner { get; set; }
        public string ReservationID { get; set; }
        public string ReservationStatus { get; set; }
        public string SecondaryOwner { get; set; }
        public string ExpirationDate { get; set; }
    }
}
