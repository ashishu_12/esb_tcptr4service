﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class ICPDetailModel
    {
        public string NumberOfActiveMembers { get; set; }
        public string Facilities { get; set; }
        public string WeeklyChildAttendance { get; set; }
        public string location { get; set; }
        public string PartneredwithCompassion { get; set; }
        public string Interventionavailable { get; set; }
    }
}
