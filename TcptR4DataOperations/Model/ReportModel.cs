﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class ReportModel
    {
        public long supporterId { get; set; }
        public string subject { get; set; }
        public string internalsubject { get; set; }
        public string country { get; set; }
        public string childkey { get; set; }
        public string needid { get; set; }
        public string beneficiaryglobalid { get; set; }
        public string mergechildname { get; set; }
        public string heshethey { get; set; }
        public string himherthem { get; set; }
        public string hishertheir { get; set; }
        public string mergefield1 { get; set; }
        public string mergefield2 { get; set; }
        public string mergefield3 { get; set; }
        public string campaignformtype { get; set; }
        public int sequence { get; set; }

    }
}
