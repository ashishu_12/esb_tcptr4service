﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class StewardshipModel
    {
       public string Giver { get; set; }
        public string Type { get; set; }
        public string Shortreference { get; set; }
        public string GeneralDetail { get; set; }
        public string GiverPostcode { get; set; }
        public string GiverNo { get; set; }
        public int actionid { get; set; }
        public long recordid { get; set; }
        public string BBID { get; set; }
        public string Amount { get; set; }
        public string Reference { get; set; }

        public string date { get; set; }

        public string filename { get; set; }
    }
}
