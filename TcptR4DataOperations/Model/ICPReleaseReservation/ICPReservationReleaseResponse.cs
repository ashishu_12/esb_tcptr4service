﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPReleaseReservation
{
    public class ICPReservationReleaseResponse
    {
        public int Status { get; set; }
        public ReservationCancelResponseList[] ReservationCancelResponseList { get; set; }
        public Error Error { get; set; }
    }
    public class ReservationCancelResponseList
    {
        public string GlobalPartner_ID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
