﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPReleaseReservation
{
    class ICPReservationReleaseRequest
    {
        public ReservationCancelRequestList[] ReservationCancelRequestList { get; set; }
    }
    [DataContract]
    public class ReservationCancelRequestList
    {
        [DataMember]
        public string ICP_ID { get; set; }
        [DataMember]
        public string GlobalPartner_ID { get; set; }
        [DataMember]
        public string Reservation_ID { get; set; }
    }
}
