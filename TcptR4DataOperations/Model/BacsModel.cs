﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class BacsModel
    {
        public int actionid { get; set; }
        public string RecordType { get; set; }
        public string Reference { get; set; }
        public string ReasonCode { get; set; }
        public string PayerName { get; set; }
        public string PayerNewName { get; set; }
        public string PayerSortCode { get; set; }
        public string PayerNewSortCode { get; set; }
        public string PayerAccountNumber { get; set; }
        public string PayerNewAccountNumber { get; set; }
        public string DueDate { get; set; }
        public string NewDueDate { get; set; }
        public string PaymentFrequency { get; set; }
        public string NewPaymentFrequency { get; set; }
        public string AmountofPayment { get; set; }
        public string AmountofNewPayment { get; set; }
        public string EffectiveDate { get; set; }
        public string LastPaymentDate { get; set; }
        public string AOSN { get; set; }
        public string Amount { get; set; }
        public long recordid { get; set; }
        public string Revenue_ID { get; set; }
        public string Date { get; set; }
    }

    public class DuplicateDetails
    {
        public int _Index { get; set; }
        public int _Amout { get; set; }
        public string AccountNo { get; set; }
        public string ReferenceNo { get; set; }
    }

    public class PaymentMethodNone
    {
        public string odatametadata { get; set; }
        public Value[] value { get; set; }
        public string odatacount { get; set; }
    }

    public class Value
    {
        public string Paymentmethod { get; set; }
        public string ConstituentLookupID { get; set; }
        public string Amount { get; set; }
        public string Installmentfrequency { get; set; }
        public string Recurringgiftnexttransactiondate { get; set; }
        public string SponsorshipsSponsorshipOpportunityChildFirstname { get; set; }
        public string Recurringgiftstatus { get; set; }
        public string BasecurrencyID { get; set; }
        public string QUERYRECID { get; set; }
    }

}
