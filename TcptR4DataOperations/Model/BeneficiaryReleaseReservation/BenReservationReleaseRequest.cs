﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryReleaseReservation
{
    public class BenReservationReleaseRequest
    {
        public ReservationCancelRequestList[] ReservationCancelRequestList { get; set; }
    }
    [DataContract]
    public class ReservationCancelRequestList
    {
        [DataMember]
        public string Beneficiary_GlobalID { get; set; }
        [DataMember]
        public string Reservation_ID { get; set; }
        [DataMember]
        public string GlobalPartner_ID { get; set; }
        
    }
}
