﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiaryReleaseReservation
{
    public class BenReservationReleaseResponse
    {
        public int Status { get; set; }
        public ReservationCancelResponseList[] ReservationCancelResponseList { get; set; }
        public Error Error { get; set; }
    }
    public class ReservationCancelResponseList
    {
        public string Reservation_ID { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
