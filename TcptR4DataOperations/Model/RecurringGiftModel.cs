﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class RecurringGiftModel
    {
        public string Frequency { get; set; }
        public string TotalAmountOfAllSponsorshipRG { get; set; }
        public string preferred_child_name { get; set; }
        public string NextInstallmentDate { get; set; }
        public string RevenueID { get; set; }
        public string ChristmasRGNextInstallmentDate { get; set; }
        public bool IsSponsorshipPlus { get; set; }
        public bool IsHillsongSponsorshipPlus { get; set; }
        public string ChildID { get; set; }
        public string RecurringGiftType { get; set; }
        public int RGAmount { get; set; }
        public string DirectDebitReferenceNumber { get; set; }
        public string StandingOrderReferenceNumber { get; set; }
        public string OtherReferenceNumber { get; set; }
        public string DDAccountNo { get; set; }
        public string DDSortCode { get; set; }
        public string RecordID { get; set; }
        public string ConstituentRecordID { get; set; }
        public string lookupid { get; set; }
        public string DDISource { get; set; }
        public string DDISourceDate { get; set; }
        public string Source { get; set; }
        public string NeedKey { get; set; }

        public DateTime BirthDate { get; set; }
    }
    public class ReferenceNumbers
    {
        public string DirectDebitReferenceNumber { get; set; }
        public string StandingOrderReferenceNumber { get; set; }
        public string OtherReferenceNumber { get; set; }
    }

    public class InitialRGData
    {
        public int actionid { get; set; }
        public string Lookupid { get; set; }
        public string Amount { get; set; }
        public string Needkey { get; set; }
        public string Designation { get; set; }
        public string Source { get; set; }
        public string boosterFlag { get; set; }
    }

}
