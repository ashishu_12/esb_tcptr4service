﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiariesReservationsCancellations
{
    public class Request
    {
        public Reservationcancelrequestlist[] ReservationCancelRequestList { get; set; }
    }

    public class Reservationcancelrequestlist
    {
        public string GlobalPartner_ID { get; set; }
        public string Reservation_ID { get; set; }
    }
}
