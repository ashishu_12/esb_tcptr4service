﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiariesReservationsCancellations
{
    public class Response
    {
        public int Status { get; set; }
        public Reservationcancelresponselist[] ReservationCancelResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Reservationcancelresponselist
    {
        public string GlobalPartner_ID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
