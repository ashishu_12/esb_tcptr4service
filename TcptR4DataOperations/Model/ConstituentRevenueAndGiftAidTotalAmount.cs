﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class ConstituentRevenueAndGiftAidTotalAmount
    {
        public string RevenueTotalAmount { get; set; }
        public string GiftAidTotalAmount { get; set; }
    }
}
