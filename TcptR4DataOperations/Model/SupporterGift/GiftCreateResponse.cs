﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.SupporterGift
{
    public class GiftCreateResponse
    {
        public int Status { get; set; }
        public Error Error { get; set; }
        public errorList[] csvValidations { get; set; }        
        public Giftcreateresponses GiftCreateResponses { get; set; }        
    }

    public class Giftcreateresponses
    {        
        public Giftcreateresponselist[] GiftCreateResponseList { get; set; }       
    }

    public class Giftcreateresponselist
    {
        public string PartnerGiftID { get; set; }
        public string PartnerGiftDate { get; set; }
        public string ThresholdViolatedType { get; set; }
        public string Id { get; set; }
        public string ExchangeRatePartnerToGMC { get; set; }
        public bool IsThresholdViolated { get; set; }
        
    }
}
