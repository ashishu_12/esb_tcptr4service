﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.SupporterGift
{
    public class GiftCreateRequest
    {
        public Giftcreaterequestlist[] GiftCreateRequestList { get; set; }
        public errorList[] errorList { get; set; }
    }

    public class errorList {
        public int rowNo { get; set; }
        public string rowData { get; set; }
        public string[] errors { get; set; }
    }

    public class Giftcreaterequestlist
    {
        public string Beneficiary_GlobalID { get; set; }
        public float AmountInOriginatingCurrency { get; set; }
        public string CurrencyCode { get; set; }
        public string GiftSubType { get; set; }
        public string GiftType { get; set; }
        public string GlobalPartnerNote { get; set; }
        public string PartnerGiftDate { get; set; }
        public string PartnerGiftID { get; set; }
        public string RecipientId { get; set; }
        public string RecipientType { get; set; }
        public string Supporter_GlobalID { get; set; }
        public string Supporter_GlobalPartnerSupporterID { get; set; }
        public string errorOutput { get; set; }
    }
}
