﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPSearch
{
    public class Response
    {
        public Icpsearchresponselist[] ICPSearchResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Icpsearchresponselist
    {
        public string ICP_ID { get; set; }
        public string ICP_Name { get; set; }
        public string FieldOffice_Name { get; set; }
        public string Territory_Name { get; set; }
        public string Cluster_Name { get; set; }
        public string FieldOffice_Country { get; set; }
        public string UnsponsoredScore { get; set; }
        public string InternationalDenominationAffiliation { get; set; }
        public string ClosestMajorCityEnglish { get; set; }
        public string AirportTravelTime { get; set; }
        public string Status { get; set; }
    }
}
