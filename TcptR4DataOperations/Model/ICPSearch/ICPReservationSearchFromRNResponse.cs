﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPSearch
{
    public class ICPReservationSearchFromRNResponse
    {
        public string ICP_ID { get; set; }
        public string ICP_Name { get; set; }
        public string FieldOffice_Name { get; set; }
        public string Territory_Name { get; set; }
        public string Cluster_Name { get; set; }
        public string FieldOffice_Country { get; set; }
        public string UnsponsoredScore { get; set; }
        public string InternationalDenominationAffiliation { get; set; }
        public string ClosestMajorCityEnglish { get; set; }
        public string AirportTravelTime { get; set; }
        public string Status { get; set; }
        public string CampaignEventIdentifier { get; set; }
        public string IsReservationAutoApproved { get; set; }
        public string Project { get; set; }
        public string ReservationType { get; set; }
        public string SourceCode { get; set; }
        public string SecondaryOwner { get; set; }
        public string ReservationStatus { get; set; }
        public string ReservationID { get; set; }
        public string PrimaryOwner { get; set; }
        public string HoldYieldRate { get; set; }
        public string HoldExpirationDate { get; set; }
        public string Channel_Name { get; set; }
        public string Beneficiary_GlobalID { get; set; }

        public string ICP_Country { get; set; }//new Params
        public string Beneficiary_LocalID { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string FullName { get; set; }
        public string ExpirationDate { get; set; }
    }
}
