﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPSearch
{
    public class ICPReservationSearchFromRNModel
    {
        public string ICPId { get; set; }
        public string ICPName { get; set; }
        public string[] Country { get; set; }
        public string ReservationID { get; set; }
        public string SourceCode { get; set; }
        public string ReservationStatus { get; set; }
        public string ShowAllReservations { get; set; }
        public string takelimit { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string ICP_Country { get; set; }//new Params
        public string Beneficiary_LocalID { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string FullName { get; set; }
    }
}
