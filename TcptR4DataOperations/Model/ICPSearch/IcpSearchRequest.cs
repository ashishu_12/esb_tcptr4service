﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.ICPSearch
{
    public class Request
    {
        public Icpsearchrequest ICPSearchRequest { get; set; }
    }

    public class Icpsearchrequest
    {
        public Icpsearchfilterdetail[] ICPSearchFilterDetails { get; set; }
        //public string Territory_Name { get; set; }
    }

    public class Icpsearchfilterdetail
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public string[] Value { get; set; }
    }
    public class IcpSearchRequestModel
    {
        public string LongWaiting { get; set; }
        public string fieldOffice { get; set; }
        public string intDenoAffiliation { get; set; }
        public string status { get; set; }
        public string[] Territory { get; set; }
        public string ICPCluster { get; set; }
        public string[] Country { get; set; }
        public string ClosestMajorCity { get; set; }
        public string ICPId { get; set; }
        public string ICPName { get; set; }
        public string HighRate { get; set; }
        public string LowRate { get; set; }
    }

}
