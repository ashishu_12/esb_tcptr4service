﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class DonationModel
    {
        public string Date { get; set; }
        public string Amount { get; set; }
        public string DonationType { get; set; }
    }
}
