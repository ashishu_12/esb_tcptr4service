﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class SupporterCommitmentModel
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DelinkType { get; set; }
        public string SupporterId { get; set; }
        public string CommitmentId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Postcode { get; set; }
        public string Need { get; set; }
        public string SupporterFirstName { get; set; }
        public string SupporterLastName { get; set; }
        public string SupporterEmail { get; set; }
        public string SupporterPostcode { get; set; }
        public string BlackBaudId { get; set; }
        public string CompassConID { get; set; }
        public string childName { get; set; }
        public string childId { get; set; }
        public string groupName { get; set; }
        public string commitmentType { get; set; }
        public string startdate { get; set; }
        public string globalCommitmentId { get; set; }
        public string globalCorrCommitmentId { get; set; }
        public string childGlobalId { get; set; }
        public string supGrpGlobalId { get; set; }
        public string SupporterGroupID { get; set; }
        public string BlackbaudID { get; set; }
        public string ExistingBeneficiaryGlobalId { get; set; }

        // only for Dep-Sub flow
        public string MarketingChannel { get; set; }
        public string RelationshipManager { get; set; }
        public string Organisation { get; set; }
        public string MarketingCode { get; set; }
        public string CommSupporter { get; set; }
        public string Event { get; set; }
        public string Campaign { get; set; }
        public string BBDDAmount { get; set; }
        public string BBDDNextDate { get; set; }
        public string BBDDStartDateText { get; set; }
        public string BBPaymentMethod { get; set; }
        public string DDReference { get; set; }
        public string SponsorshipPlus { get; set; }
        public string PaymentMethod { get; set; }
        public string LinkedToPartnership { get; set; }

        public string CountryName { get; set; }
        public string Beneficiary_LocalID { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Hold_ID { get; set; }
        public string age { get; set; }
        public string Country { get; set; }
        public string StartDate { get; set; }
        public string ICP_ID { get; set; }
        public int Amount { get; set; }
        public string RecurringGiftType { get; set; }
        public string endDate { get; set; }
        public string ChildExitDate { get; set; }
        public string RevenueId { get; set; }
        public string PrimarySupporterblackbaudid { get; set; }
        public string SupGrpType { get; set; }
        public string SupporterGroupBlackbaudid { get; set; }
    }
    public class CommitmentData
    {
        public string CommitmentID { get; set; }
        public string GlobalCommitmentID { get; set; }
        public string GlobalCorrespCommitmentID { get; set; }
        public string ChildGlobalID { get; set; }
        public string SponsorshipID { get; set; }
         public string ChildPreferedName { get; set; }

    }
}
