﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class ErrorResponse
    {
        public Error Error { get; set; }
    }

    public class Error
    {
        public string ErrorId { get; set; }
        public DateTime ErrorTimestamp { get; set; }
        public string ErrorClass { get; set; }
        public string ErrorCategory { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorRetryable { get; set; }
        public string ErrorModule { get; set; }
        public string ErrorSubModule { get; set; }
        public string ErrorMethod { get; set; }
        public string ErrorLoggedInUser { get; set; }
        public string RelatedRecordId { get; set; }
    }
}
