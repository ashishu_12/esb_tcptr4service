﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class BarclaysStandingOrderModel
    {
        public int actionid { get; set; }
        public string Accountno { get; set; }
        public string Sortcode { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string BBID { get; set; }
        public string accountno_sortcode { get; set; }
        public long recordid { get; set; }
        public string constituentName { get; set; }
        public string referenceno { get; set; }
        public string beforedays { get; set; }
        public string afterdays { get; set; }
        public string filename { get; set; }
        public bool isContainImpactGiving { get; set; }
        public string StandingOrderDecription { get; set; }
    }
}
