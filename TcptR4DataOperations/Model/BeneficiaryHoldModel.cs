﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class BeneficiaryHoldModel
    {
        public string BeneficiaryState { get; set; }
        public object EstimatedNoMoneyYieldRate { get; set; }
        public DateTime HoldEndDate { get; set; }
        public string HoldID { get; set; }
        public string HoldYieldRate { get; set; }
        public bool IsSpecialHandling { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SourceCode { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string Beneficiary_LocalID { get; set; }
        public string Channel_Name { get; set; }
        public string ChannelId { get; set; }
        public string GlobalPartner_ID { get; set; }
        public int HoldStatus { get; set; }
        public long RNneedID { get; set; }
    }
}
