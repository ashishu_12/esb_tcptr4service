﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiarySearch
{
    public class Request
    {
        public Beneficiarysearchrequestlist BeneficiarySearchRequestList { get; set; }
    }

    public class Beneficiarysearchrequestlist
    {
        public int NumberOfBeneficiaries { get; set; }
        public object SortBy { get; set; }
        public object SortType { get; set; }
        public int Start { get; set; }
        public Filter[] Filter { get; set; }
    }

    public class Filter
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public string[] Value { get; set; }
    }
}
