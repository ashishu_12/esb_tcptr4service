﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiarySearch
{
    public class Response
    {
        public string NumberOfBeneficiaries { get; set; }
        public Beneficiarysearchresponselist[] BeneficiarySearchResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Beneficiarysearchresponselist  
    {
        public string Beneficiary_ThumbnailURL { get; set; }
        public string FullBodyImageURL { get; set; }
        public string FullName { get; set; }
        public string PriorityScore { get; set; }
        public string BeneficiaryState { get; set; }
        public string FieldOffice_Name { get; set; }
        public string ICP_Country { get; set; }
        public string ICP_ID { get; set; }
        public string Age { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string LocalNumber { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string Beneficiary_LocalID { get; set; }
        public string HoldingGlobalPartnerID { get; set; }
        public string HoldExpirationDate { get; set; }
        public string CorrespondentScore { get; set; }
        public string PreferredName { get; set; }
        public string WaitingSinceDate { get; set; }
        public bool IsInHIVAffectedArea { get; set; }
        public bool IsSpecialNeeds { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsOrphan { get; set; }
        public string MinDaysWaiting { get; set; }
        public string SourceCode { get; set; }
        public string HoldId { get; set; }
        public string LongWaiting{ get; set; }
    }
}
