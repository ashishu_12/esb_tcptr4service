﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiarySearch
{
    class BeneficiaryDataModel
    {
        public int localId { get; set; }
        public string gender { get; set; }
        public string countries { get; set; }
        public int minPriorityScore { get; set; }
        public string name { get; set; }
        public string icpId { get; set; }
        public string birthDateFrom { get; set; }
        public string birthDateTo { get; set; }
        public int minDaysWaiting { get; set; }
        public string includeOnHold { get; set; }
        public string status { get; set; }
        public int hasSpecialNeeds { get; set; }
        public string hasSiblings { get; set; }
        public string isFatherAlive { get; set; }
        public string isMotherAlive { get; set; }
        public string cluster { get; set; }
        public string isPhysicalDisable { get; set; }
        public int hivAffectedArea { get; set; }
        public string chronicIllness { get; set; }
        public string isTransitioned { get; set; }
        public string globalId { get; set; }
        public int take { get; set; }
        public int skip { get; set; }
    }
}
