﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiarySearch
{
    //public class Request
    //{
    //    public Beneficiarysearchrequestparamslist BeneficiarySearchRequestList { get; set; }
    //}

    //public class Beneficiarysearchrequestparamslist
    //{
    //    public int NumberOfBeneficiaries { get; set; }
    //    public object SortBy { get; set; }
    //    public object SortType { get; set; }
    //    public int Start { get; set; }
    //    public Filter[] Filter { get; set; }
    //}

    //public class Filter
    //{
    //    public string Field { get; set; }
    //    public string Operator { get; set; }
    //    public string[] Value { get; set; }
    //}
    class BeneficiarySearchByParams
    {
        public string LongWaiting { get; set; }
        public string localId { get; set; }
        public string gender { get; set; }
        public string countries { get; set; }
        public string minPriorityScore { get; set; }
        public string name { get; set; }
        public string icpId { get; set; }
        public string birthDateFrom { get; set; }
        public string birthDateTo { get; set; }
        public string minDaysWaiting { get; set; }
        public string includeOnHold { get; set; }
        public string status { get; set; }
        public string hasSpecialNeeds { get; set; }
        public string hasSiblings { get; set; }
        public string isFatherAlive { get; set; }
        public string isMotherAlive { get; set; }
        public string cluster { get; set; }
        public string isPhysicalDisable { get; set; }
        public string hivAffectedArea { get; set; }
        public string chronicIllness { get; set; }
        public string isTransitioned { get; set; }
        public string globalId { get; set; }
        public int take { get; set; }
        public int skip { get; set; }
        public string SourceCode { get; set; }
        public string HoldingGlobalPartnerID { get; set; }
        
    }
}
