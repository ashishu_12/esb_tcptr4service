﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class DDConnectResponse
    {
        public string Error { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        
    }
}
