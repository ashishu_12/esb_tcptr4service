﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.BeneficiariesHold
{
    public class Requests
    {
        public Beneficiaryholdrequestlist[] BeneficiaryHoldRequestList { get; set; }
    }

    public class Beneficiaryholdrequestlist
    {
        public string BeneficiaryState { get; set; }
        public string EstimatedNoMoneyYieldRate { get; set; }
        public string HoldEndDate { get; set; }
        public string HoldID { get; set; }
        public string HoldYieldRate { get; set; }
        public bool IsSpecialHandling { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SourceCode { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string Channel_Name { get; set; }
        public string GlobalPartner_ID { get; set; }
    }
}
