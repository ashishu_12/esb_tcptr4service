﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.CreateCorrespondentCommitment.CancelCorrespodentCommitment
{
    public class Resposne
    {
        public int Status { get; set; }
        public Correspondentcancellationlistresponse[] CorrespondentCancellationListResponse { get; set; }
        public Error Error { get; set; }
    }

    public class Correspondentcancellationlistresponse
    {
        public string CorrespondentCommitmentID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string EndDate { get; set; }
        public string Beneficiary_GlobalID { get; set; }
    }
}
