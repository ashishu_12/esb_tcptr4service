﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.CreateCorrespondentCommitment.CancelCorrespodentCommitment
{
    public class Request
    {
        public Correspondentcancellationlist[] CorrespondentCancellationList { get; set; }
    }

    public class Correspondentcancellationlist
    {
        public string CorrespondentCommitmentID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public string Beneficiary_GlobalID { get; set; }
    }
}
