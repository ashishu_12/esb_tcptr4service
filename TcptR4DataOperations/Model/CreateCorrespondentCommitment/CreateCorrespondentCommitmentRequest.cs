﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.CreateCorrespondentCommitment
{
    public class Request
    {
        public Correspondentlist[] CorrespondentList { get; set; }
    }

    public class Correspondentlist
    {
        public object CorrespondenceEndDate { get; set; }
        public string CorrespondentCorrespondenceLanguage { get; set; }
        public string CorrespondentGlobalPartnerID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public string PrimaryHoldOwner { get; set; }
        public string SecondaryHoldOwner { get; set; }
        public string SponsorGlobalPartnerID { get; set; }
        public string Beneficiary_GlobalID { get; set; }
        public string Channel_Name { get; set; }
    }
}
