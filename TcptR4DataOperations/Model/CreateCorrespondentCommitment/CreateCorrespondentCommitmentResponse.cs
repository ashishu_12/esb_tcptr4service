﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model.CreateCorrespondentCommitment
{
    public class Response
    {
        public int Status { get; set; }
        public Correspondentcreateresponselist[] CorrespondentCreateResponseList { get; set; }
        public Error Error { get; set; }
    }

    public class Correspondentcreateresponselist
    {
        public string Beneficiary_GlobalID { get; set; }
        public string CorrespondentSupporterGlobalID { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string CorrespondentCommitmentID { get; set; }
        public string StartDate { get; set; }
    }
}
