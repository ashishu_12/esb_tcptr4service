﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class TokenResponseContent
    {
        public string access_token { get; set; }
        public long expires_in { get; set; }
        public string token_type { get; set; }
    }
    public class EightByEightResponceContent
    {
        public recordings recordings { get; set; }
    }
    public class recordings
    {
        public List<file> file { get; set; }
    }

    public class file
    {
        public string startDate { get; set; }
        public string agentID { get; set; }
        public string phoneNumber { get; set; }
        public string channel { get; set; }
        public string caseID { get; set; }
        public string duration { get; set; }
        public string filename { get; set; }
        public string queueID { get; set; }
        public string queueName { get; set; }
        public string accountNumber { get; set; }
        public string extVar1 { get; set; }
        public string extVar2 { get; set; }
        public string outDialCode { get; set; }
        public string wrapUpCode { get; set; }
        public string transactionCodes { get; set; }
        public string blackbaudid { get; set; }
        public long RecordId { get; set; }
    }

    public class recording
    {
        public string phoneNumber { get; set; }
        public string filename { get; set; }
        public string startDate { get; set; }
        public string blackbaudid { get; set; }
    }


    public class FCMJson
    {
        public message message { get; set; }
    }

    public class message
    {
        public string condition { get; set; }
        public noti notification { get; set; }
        public noti data { get; set; }
        public string destination { get; set; }
    }

    public class noti
    {   
        public string body { get; set; }
        public string title { get; set; }
    }

}
