﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class BacsPaymentModel
    {
        public string Paymentmethod { get; set; }
        public string Blackbaudid { get; set; }
        public string Revenueid { get; set; }
        public string Amount { get; set; }
        public string Accountsystem { get; set; }
        public string SupporterId { get; set; }
        public string InstalmentFrequency { get; set; } 
        public string NextTransactionDate { get; set; }
        public string RG_Status { get; set; }

    }

    public class IncidentThreadText1
    {
        public string SupporterId { get; set; }
        public string Paymentmethod { get; set; }
        public string Blackbaudid { get; set; }
        public string Revenueid { get; set; }
        public string Amount { get; set; }

    }

    public class IncidentValue
    {
        public IncidentThreadText1 ThreadData { get; set; }
    }

    public class PaymentMethodNoneOdata
    {
        public string odatametadata { get; set; }
        public PaymentMethodNoneFields[] value { get; set; }
        public string odatacount { get; set; }
    }

    public class PaymentMethodNoneFields
    {
        public string Paymentmethod { get; set; }
        public string ConstituentLookupID { get; set; }
        public string RevenueID { get; set; }
        public string Amount { get; set; }
        public string Accountsystem { get; set; }
        public string Installmentfrequency { get; set; }
        public string Recurringgiftnexttransactiondate { get; set; }
        public string CancelledViaBACSAttributeValue { get; set; }
        public string DeceasedAttributeValue { get; set; }
        public string SponsorshipsSponsorshipOpportunityChildFirstname { get; set; }

        public string SystemrecordID { get; set; }
        public string BasecurrencyID { get; set; }
        public string QUERYRECID { get; set; }
    }

    public class PaymentMethodNoneIncidentData
    {
        public long SupporterID { get; set; }
        public string EmailSubject { get; set; }
        public string InternalSubject { get; set; }
        public int CampaignFormType { get; set; }
        public int Status { get; set; }
        public string ResolutionDueDate { get; set; }
        public int ContactAttempt { get; set; }
    }

}
