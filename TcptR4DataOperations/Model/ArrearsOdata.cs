﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class ArrearsOdata
    {
        public string odatametadata { get; set; }
        public Vals[] value { get; set; }
        public string odatacount { get; set; }
    }

    public class Vals
    {
        public string ConstituentLookupID { get; set; }
        public string SUMAmount { get; set; }
        public string Accountsystem { get; set; }
        public string Installmentfrequency { get; set; }
        public string Recurringgiftnexttransactiondate { get; set; }
        public string SponsorshipsSponsorshipOpportunityChildFirstname { get; set; }
        public string BasecurrencyID { get; set; }
    }

}
