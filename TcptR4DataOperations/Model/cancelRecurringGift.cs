﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class cancelRecurringGift
    {
        public string commBBID { get; set; }
        public string sponsorshipType { get; set; }
        public string childlocalid { get; set;}
        public string conid { get; set; }
        public string revenueid { get; set; }
        public string Responce { get; set; }
        public string PrimarySupporterblackbaudid { get; set; }
    }

    public class RGDetails
    {
        public string RGAmount { get; set; }
        public string RGType { get; set; }
    }
}
