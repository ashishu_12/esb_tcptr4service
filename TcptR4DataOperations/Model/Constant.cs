﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcptR4DataOperations.Model
{
    public class Constant
    {
        public const string INSTALLMENTFREQUENCY = "Monthly";
        public const string PAYMENTMETHOD = "Direct Debit";
    }
}
